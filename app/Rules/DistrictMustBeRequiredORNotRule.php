<?php

namespace App\Rules;

use App\Models\City;
use App\Models\District;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class DistrictMustBeRequiredORNotRule implements Rule
{
    private $city;
    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($dis_id)
    {
        $this->city = District::where('id', $dis_id)->with('city')->first();
        Log::info($this->city);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        Log::info(!is_int($value));
        if ($this->city && !$value) {
            $this->message = 'The :attribute required';
            return false;
        } elseif ($this->city && !is_numeric($value)) {
            $this->message = 'The :attribute required';
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
