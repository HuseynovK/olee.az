<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class updatediscount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_discount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Discount in cron 1 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \App\Models\Product::whereDate('discount_expires_at', '<', \Carbon\Carbon::now())->update([
            'discount' => null,
            'discount_type' => null,
            'discount_expires_at' => null
        ]);
        return 1;
    }
}
