<?php

namespace App\Broadcasting;

use App\Services;
use App\Models\User;
use Illuminate\Notifications\Notification;
use App\Notifications\SmsMessage;

class AtlSms
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\Models\User  $user
     * @return array|bool
     */
    public function join(User $user)
    {
        //
    }

    /**
     * @param $notifiable
     * @param Notification $notification
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toAtlSms($notifiable);

        (new Services\AtlSms())->sendSms(
            $message->getRecipient(),
            $message->getContent()
        );
    }
}
