<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class ProductAttribute extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'product_attributes';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'input'  => 'Text',
        'select' => 'Select',
        'file'   => 'File',
    ];

    protected $fillable = [
        'product_id',
        'type',
        'title',
        'value',
        'price',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function productAttributeProductAttributeValues()
    {
        return $this->hasMany(ProductAttributeValue::class, 'product_attribute_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
