<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersOrderStatus extends Model
{
    use HasFactory;

    protected $table = 'orders_order_statuses';

    protected $fillable = [
        'order_id',
        'order_status_id',
        'number',
        'title',
        'status',
        'completed_at'
    ];

    public function order_status()
    {
        return $this->belongsTo(OrderStatus::class);
    }
}
