<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Coupon extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'coupons';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'expire_date',
    ];

    protected $appends = [
        'is_available'
    ];

    const DISCOUNT_TYPE_SELECT = [
        'flat'       => 'Flat',
        'percentage' => '%',
    ];

    const COUPON_TYPE_SELECT = [
        'count'       => 'Usage count',
        'expire_date'    => 'Until date',
    ];

    protected $fillable = [
        'title',
        'code',
        'discount',
        'discount_type',
        'coupon_type',
        'count',
        'expire_date',
        'used_times',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * @return bool
     */
    public function getIsAvailableAttribute(): bool
    {
        if ($this->getAttribute('coupon_type') == 'count')
        {
            return $this->getAttribute('count') > $this->getAttribute('used_times');
        }
        if ($this->getAttribute('coupon_type') == 'expire_date')
        {
            return $this->getAttribute('expire_date') > date("Y-m-d");
        }

        return false;
    }
}
