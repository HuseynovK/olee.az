<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'carts';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'session',
        'coupon_id',
        'total',
        'payed',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = ['coupon_final_discount'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function cartCartItems()
    {
        return $this->hasMany(CartItem::class, 'cart_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id');
    }

    public function calculate(): void
    {
        $items = CartItem::query()
            ->where('cart_id', $this->getAttribute('id'))
            ->get();
        $payed = 0;
        $total = 0;
        foreach ($items as $item) {
            $payed += $item->getAttribute('final_price');
            $total += $item->getAttribute('price') * $item->getAttribute('quantity');
        }

        // coupon calculation
        if ($this->getAttribute('coupon_id')) {
            $coupon = Coupon::query()->find($this->getAttribute('coupon_id'));

            if ($coupon->is_available) {
                if ($coupon->getAttribute('discount_type') == 'percentage') {
                    $payed = ($payed * (100 - $coupon->getAttribute('discount'))) / 100;
                }

                if ($coupon->getAttribute('discount_type') == 'flat') {
                    $payed = $payed - $coupon->getAttribute('discount');
                }
            } else {
                $this->setAttribute('coupon_id', null);
            }
        }

        $this->setAttribute('payed', $payed);
        $this->setAttribute('total', $total);
        $this->save();

//        if ($total == 0)
//            $this->delete();
    }

    public function getCouponFinalDiscountAttribute()
    {
        $discount = 0;

        if (!$this->getAttribute('coupon_id'))
            return $discount;

        $coupon = Coupon::query()->find($this->getAttribute('coupon_id'));

        if ($coupon->is_available) {
            if ($coupon->getAttribute('discount_type') == 'percentage') {
                $discount = ($this->getAttribute('payed') * $coupon->getAttribute('discount')) / 100;
            }

            if ($coupon->getAttribute('discount_type') == 'flat') {
                $discount = $coupon->getAttribute('discount');
            }
        }

        return (float)number_format($discount, 2, ',', '');
    }
}
