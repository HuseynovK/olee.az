<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use \DateTimeInterface;
use Illuminate\Support\Facades\Auth;

class Category extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia, HasFactory;

    public $table = 'categories';

    protected $appends = [
        'image',
        'navbar',
        'full_title'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'sira',
        'parent_id',
        'is_published',
        'is_featured',
        'created_at',
        'updated_at',
        'deleted_at',
        'meta_keywords',
        'seo_title',
        'seo_desc',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function categoryProducts()
    {
        return $this->hasMany(Product::class, 'category_id', 'id')->select(['products.*']);
    }

    public function featuredProducts()
    {
        return $this->hasMany(Product::class, 'category_id', 'id')
            ->where(['is_featured' => true, 'is_published' => true])->limit(4);
    }

    public function limitRelationship(string $name, int $limit)
    {
        $this->relations[$name] = $this->relations[$name]->slice(0, $limit);
    }


    public function categoryCategoryAttributes()
    {
        return $this->hasOne(CategoryAttribute::class, 'category_id', 'id');
    }

    public function getImageAttribute()
    {
        $file = $this->getMedia('image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
        }

        return $file;
    }

    public function getNavbarAttribute()
    {
        $file = $this->getMedia('navbar')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
        }

        return $file;
    }

    public function getFullTitleAttribute()
    {
        return "{$this->parent?->title} - {$this->title}";
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }
}
