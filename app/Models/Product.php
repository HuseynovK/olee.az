<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use \DateTimeInterface;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class Product extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia, HasFactory;

    public $table = 'products';

    protected $appends = [
        'thumbnail',
        'images',
        'retailer_id',
    ];

    const DISCOUNT_TYPE_SELECT = [
        'flat'       => 'Flat',
        'percentage' => '%',
    ];

    const PREPERATION_TIME_UNIT_SELECT = [
        'hours' => 'Hours',
        'days'  => 'Days',
    ];

    protected $dates = [
        'discount_expires_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'category_id',
        'title',
        'description',
        'delivery',
        'price',
        'discount',
        'discount_type',
        'discount_expires_at',
        'preperation_time',
        'preperation_time_unit',
        'is_published',
        'is_featured',
        'is_woman_day',
        'is_valentine',
        'is_most_visited',
        'is_work',
        'is_congrotulation',
        'is_apologize',
        'is_love',
        'recommendation_impression_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'is_birthday',
        'is_for_love',
        'is_be_sad',
        'is_child',
        'is_anniversary',
        'is_own',
        'is_opening',
        'is_wedding',
        'seo_title',
        'seo_desc',
    ];


    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('small')->fit('crop', 80, 80);
        $this->addMediaConversion('thumb')->fit('crop', 264, 264);
        $this->addMediaConversion('preview')->fit('crop', 552, 552);
    }

    public function productProductVariations()
    {
        return $this->hasMany(ProductVariation::class, 'product_id', 'id');
    }

    public function productProductAttributes()
    {
        return $this->hasMany(ProductAttribute::class, 'product_id', 'id');
    }

    public function recommendationImpressionProducts()
    {
        return $this->hasMany(Product::class, 'recommendation_impression_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getDiscountExpiresAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setDiscountExpiresAtAttribute($value)
    {
        $this->attributes['discount_expires_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function getRetailerIdAttribute($value)
    {
        return $value ?: str_replace("-", "_", Str::slug(strtolower($this->category->title))) . '_' . $this->id;
    }

    public function getProductLinkAttribute($value)
    {
        return "{$this->id}/" . Str::slug(strtolower($this->title));
    }

    public function colors()
    {
        return $this->belongsToMany(Color::class);
    }

    public function scopeWishlists($query)
    {
        if (Auth::check()) {
            return $query->leftJoin('wishlists', function ($join) {
                return $join->on('products.id', '=', 'wishlists.product_id')->where('wishlists.user_id', Auth::user()->id);
            });
        }
    }

    public function getThumbnailAttribute()
    {
        $file = $this->getMedia('thumbnail')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
        }

        return $file;
    }

    public function getImagesAttribute()
    {
        $files = $this->getMedia('images');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
            $item->preview   = $item->getUrl('preview');
        });

        return $files;
    }

    public function recommendation_impression()
    {
        return $this->belongsTo(Product::class, 'recommendation_impression_id');
    }
}
