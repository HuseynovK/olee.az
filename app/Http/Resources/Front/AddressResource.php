<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "city_id" => $this->city_id,
            "detail" => $this->detail,
            "district_id" =>$this->district_id,
            "full_name" => $this->full_name,
            "phone" => $this->phone,
            "title" => $this->title,
            "created_at" => $this->created_at->format('d M Y'),
            'html' => view('front.cart.address-card', ['address' => $this])->render()
        ];
    }
}
