<?php

namespace App\Http\View\Composer;

use App\Models\Cart;
use App\Models\Category;
use App\Models\City;
use App\Models\District;
use App\Models\Story;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class FrontComposer
{
    protected $_Language, $_User;

    public function __construct()

    {
    }

    public function compose(View $view)
    {
        $_categories = Category::with([
            'children' => function ($query) {
                return $query->where(['is_published' => true]);
            }
        ])->where(['is_published' => true, 'parent_id' => null])->get();

        $_stories = Story::where(['is_published' => true])->limit(10)->get();

        $_cities = City::query()->get();
        $_districts = District::query()->get();

        $view->with('_categories', $_categories);
        $view->with('_cities', $_cities);
        $view->with('_districts', $_districts);
        $view->with('_stories', $_stories);
    }
}
