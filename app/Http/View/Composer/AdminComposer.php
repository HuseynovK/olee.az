<?php


namespace App\Http\View\Composer;

use App\Models\Order;
use App\Models\User;
use Illuminate\View\View;

class AdminComposer
{
    protected int $unseenOrdersCount;

    public function __construct()
    {
        $this->unseenOrdersCount = 0;
    }

    public function compose(View $view)
    {
        $user = User::find(auth()->user()->id);
        $orders =  Order::query()->where('seen', false);
        if ($user->is_affiliate) {
            $orders->where('ref_id', $user->id);
        }
        $this->unseenOrdersCount = $orders->count();
        $view->with('new_orders', $this->unseenOrdersCount);
    }
}
