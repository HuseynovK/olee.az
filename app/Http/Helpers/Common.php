<?php

//highlights the selected navigation on admin panel

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

if (!function_exists('areActiveRoutes')) {
    function areActiveRoutes(array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }
        }
        return false;
    }
}

/**
 * Open Translation File
 * @return Response
 */
if (!function_exists('openJSONFile')) {
    function openJSONFile($code)
    {
        $jsonString = [];
        if (File::exists(base_path('resources/lang/' . $code . '.json'))) {
            $jsonString = file_get_contents(base_path('resources/lang/' . $code . '.json'));
            $jsonString = json_decode($jsonString, true);
        }
        return $jsonString;
    }
}

/**
 * Save JSON File
 * @return Response
 */
if (!function_exists('saveJSONFile')) {
    function saveJSONFile($code, $data)
    {
        ksort($data);
        $jsonData = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        file_put_contents(base_path('resources/lang/' . $code . '.json'), stripslashes($jsonData));
    }
}

if (!function_exists('localRoute')) {
    function localRoute($routeName, $locale = null)
    {
        if (!$locale && Auth::user()) {
            //$locale = Auth::user()->lang;
        }

        //return $locale ? LaravelLocalization::getLocalizedURL($locale, route($routeName)) : route($routeName);
    }
}

if (!function_exists('truncate')) {
    function truncate($string, $length, $stopanywhere = false)
    {
        if (strlen($string) > $length) {
            $string = substr($string, 0, ($length - 3));
            if ($stopanywhere) {
                $string .= '...';
            } else {
                $string = substr($string, 0, strrpos($string, ' ')) . '...';
            }
        }
        return $string;
    }
}

if (!function_exists('translate')) {

    function translate($word = '')
    {
        // $lang = app()->getLocale();
        return trans('global.' . $word);
    }
}

if (!function_exists('getIp')) {
    function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
}

if (!function_exists('round_up')) {
    function round_up($value, $precision = 2)
    {
        $pow = pow(10, $precision);
        return (ceil($pow * $value) + ceil($pow * $value - ceil($pow * $value))) / $pow;
    }
}

//commit
if (!function_exists('inAWishList')) {
    function inAWishList($pid)
    {
//        \Illuminate\Support\Facades\Log::info("not5 AUTH WISH");
        if (Auth::check()) {
            $wi = \App\Models\Wishlist::where('user_id', \auth()->id())->where('product_id', $pid)->first();
            if ($wi) {
//                \Illuminate\Support\Facades\Log::info("AUTH WISH");
                return true;
            }
        } elseif (session(['basket_token'])) {
            $user = \App\Models\User::where('session', session(['basket_token']))->first();
//            \Illuminate\Support\Facades\Log::info("not3 AUTH WISH");
            if ($user) {
                $wi = \App\Models\Wishlist::where('user_id', $user->id)->where('product_id', $pid)->first();
//                \Illuminate\Support\Facades\Log::info("not AUTH WISH");
                if ($wi) {
//                    \Illuminate\Support\Facades\Log::info("not 2 AUTH WISH");
                    return true;
                }
            }
        }

        return false;
    }
}
