<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Category;
use App\Models\City;
use App\Models\Product;
use App\Services\Cart;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */

    public function index()
    {
        $campaigns = Campaign::with(['media'])->where(['is_featured' => true, 'is_published' => true])->get();
        $categories = Category::with([
            'featuredProducts',
            'media',
            'featuredProducts.media',
        ])
            ->where(['is_published' => true])
            ->whereNull('parent_id')
            ->orderBy('sira')
            ->get()->each(function ($category, $index) {
                $category->featuredProducts = Product::query()
                    ->where(['is_featured' => true, 'is_published' => true])
                    ->whereIn('category_id', function ($query) use ($category) {
                        $query->select('id')->from('categories')
                            ->where('parent_id', $category->id)
                            ->orWhere('id', $category->id);
                    })->limit(4)->get();
            });

        return view('front.index', compact('campaigns', 'categories'));
    }

    public function index2()
    {
        $campaigns = Campaign::with(['media'])->where(['is_featured' => true, 'is_published' => true])->get();
        $categories = Category::with([
            'featuredProducts',
            'media',
            'featuredProducts.media',
        ])
            ->where(['is_published' => true])
            ->whereNull('parent_id')
            ->orderBy('sira')
            ->get()->each(function ($category, $index) {
                $category->featuredProducts = Product::query()
                    ->where(['is_featured' => true, 'is_published' => true])
                    ->whereIn('category_id', function ($query) use ($category) {
                        $query->select('id')->from('categories')
                            ->where('parent_id', $category->id)
                            ->orWhere('id', $category->id);
                    })->limit(4)->get();
            });
        $new_products = Product::orderByDesc('created_at')->limit(10)->get();
        $most_seller = Product::where('is_most_visited', 1)->inRandomOrder()->limit(10)->get();

        return view('front.index2', compact('campaigns', 'categories', 'new_products', 'most_seller'));
    }

    public function search(Request $request)
    {
        $searchText = trim($request->get('query'));
        $cid = explode('-', $request->get('category'));
        $category = Category::where(['id' => end($cid), 'is_published' => true])->first();

        $subcategories = collect();

        $products = Product::query()->with(['media'])
            ->when($request->filled('subcategory_id'), function (Builder $query) use ($request) {
                return $query->where('category_id', $request->get('subcategory_id'));
            })
            ->when($category, function (Builder $query) use ($category) {
                return $query->whereIn('category_id', function ($query) use ($category) {
                    $query->select('id')->from('categories')
                        ->where('parent_id', $category->id)->orWhere('id', $category->id)
                        ->where('is_published', true);
                });
            })
            ->when($searchText && $searchText != '', function (Builder $query) use ($searchText) {
                return $query->where(function ($query) use ($searchText) {
                    return $query->where('title', 'like', "%{$searchText}%");
                });
            })
            ->when($request->filled('order_by'), function (Builder $query) use ($request) {
                list($field, $direction) = explode(".", $request->get('order_by'));
                return $query->orderBy($field, $direction);
            })
            ->when($request->filled('price_range'), function (Builder $query) use ($request) {
                list($min, $max) = explode(".", $request->get('price_range'));
                if ($min != null) {
                    $query->where('price', '>=', $min);
                }
                if ($max != null) {
                    $query->where('price', '<=', $max);
                }
            })
            ->orderBy('is_featured')
            ->get();
        //->paginate(15);

        if ($request->ajax()) {
            return '';
            $renderedView = '';
            foreach ($products as $product) {
                $renderedView .= \view('front.products.product-card', ['product' => $product])->render();
            }
            return response()->json($renderedView, 200);
        }

        if ($category) {
            //            $related = Product::query()
            //                ->where('category_id', '!=', $category->id)
            //                ->limit($products->count() / 4)
            //                ->inRandomOrder()->get();
            //            $products = $products->merge($related)->shuffle();

            $subcategories = Category::query()->where('parent_id', $category->id)->get();
        }

        return view('front.products.index', compact('products', 'category', 'subcategories'));
    }

    public function product($id, $alias = "")
    {
        if (request()->filled('ref_id')) {
            Session::put('ref_id', request()->get('ref_id'));
        }
        $product = Product::with('media', 'category', 'colors')->findOrFail($id);
        $related = Product::with(['category', 'media'])->where(['category_id' => $product->category_id])->where('id', '!=', $product->id)->limit(4)->get();
        $suggestedProduct = Product::query()
            ->where('id', '!=', $id)
//            ->whereNotIn('id', $related->pluck('id'))
//            ->inRandomOrder()
            ->first();
        $cities = City::with('cityDistricts')->get();
        return view('front.products.detail', compact('product', 'related', 'suggestedProduct', 'cities'));
    }

    public function product2($id, $alias = "")
    {
        if (request()->filled('ref_id')) {
            Session::put('ref_id', request()->get('ref_id'));
        }
        $product = Product::with('media', 'category', 'colors')->findOrFail($id);
        $related = Product::with(['category', 'media'])->where(['category_id' => $product->category_id])->where('id', '!=', $product->id)->limit(4)->get();
        $suggestedProduct = Product::query()
            ->where('id', '!=', $id)
//            ->whereNotIn('id', $related->pluck('id'))
//            ->inRandomOrder()
            ->first();
        $cities = City::with('cityDistricts')->get();
        return view('front.products.detail2', compact('product', 'related', 'suggestedProduct', 'cities'));
    }

    public function calculatePrice(Request $request, $id, $alias = "")
    {
        $price = (new Cart())->calculate($id, $request);
        return response()->json(['price' => $price['payable']]);
    }

    public function valentine(Request $request)
    {
        $category = new Category(['title' => "Sevgililər Günü"]);
        $products = Product::query()->with(['media'])
            ->when($request->filled('order_by'), function (Builder $query) use ($request) {
                list($field, $direction) = explode(".", $request->get('order_by'));
                return $query->orderBy($field, $direction);
            })
            ->when($request->filled('price_range'), function (Builder $query) use ($request) {
                list($min, $max) = explode(".", $request->get('price_range'));
                if ($min != null) {
                    $query->where('price', '>=', $min);
                }
                if ($max != null) {
                    $query->where('price', '<=', $max);
                }
            })
            ->where('is_valentine', 1)
            ->orderBy('is_featured')
            ->get();


        return view('front.products.valentine', compact('products', 'category'));
    }

    public function womanDay(Request $request)
    {
        $category = new Category(['title' => "8 Mart"]);
        $products = Product::query()->with(['media'])
            ->when($request->filled('order_by'), function (Builder $query) use ($request) {
                list($field, $direction) = explode(".", $request->get('order_by'));
                return $query->orderBy($field, $direction);
            })
            ->when($request->filled('price_range'), function (Builder $query) use ($request) {
                list($min, $max) = explode(".", $request->get('price_range'));
                if ($min != null) {
                    $query->where('price', '>=', $min);
                }
                if ($max != null) {
                    $query->where('price', '<=', $max);
                }
            })
            //->where('category_id', 2)
            //->where('price', '>=', 80)
            ->where('is_women_day', true)
            ->orderBy('is_featured')
            ->get();


        return view('front.products.woman_day', compact('products', 'category'));
    }

    public function categories(Request $request, string $category, string $subCategory = null)
    {
        $searchText = trim($request->get('query'));
        $cid = explode('-', $category);

        if ($subCategory != null) {
            $subCategoryId = explode('-', $subCategory);
            $subCategory = Category::query()
                ->where(['id' => end($subCategoryId), 'is_published' => true])
                ->first();
        }

        $category = Category::query()
            ->where(['id' => end($cid), 'is_published' => true])
            ->first();
        $subcategories = collect();

        $products = Product::query()->with(['media'])
            ->when($subCategory, function (Builder $query) use ($subCategory) {
                return $query->where('category_id', $subCategory->id);
            })
            ->when($category, function (Builder $query) use ($category) {
                return $query->whereIn('category_id', function ($query) use ($category) {
                    $query->select('id')->from('categories')
                        ->where('parent_id', $category->id)->orWhere('id', $category->id)
                        ->where('is_published', true);
                });
            })
            ->when($searchText && $searchText != '', function (Builder $query) use ($searchText) {
                return $query->where(function ($query) use ($searchText) {
                    return $query->where('title', 'like', "%{$searchText}%");
                });
            })
            ->when($request->filled('order_by'), function (Builder $query) use ($request) {
                list($field, $direction) = explode(".", $request->get('order_by'));
                return $query->orderBy($field, $direction);
            })
            ->when($request->filled('price_range'), function (Builder $query) use ($request) {
                list($min, $max) = explode(".", $request->get('price_range'));
                if ($min != null) {
                    $query->where('price', '>=', $min);
                }
                if ($max != null) {
                    $query->where('price', '<=', $max);
                }
            })
            ->orderBy('is_featured')
            ->get();
        //->sortByDesc(function($prtct) {
        //    return $prtct->category->id;
        //});
        //->paginate(15);


        /* dd($products->map(function($product){
            return $product->id .'-'. $product->category->sira;
        })); */

        if ($request->ajax()) {
            return '';
            $renderedView = '';

            foreach ($products as $product) {
                $renderedView .= \view('front.products.product-card', ['product' => $product])->render();
            }

            return response()->json($renderedView, 200);
        }

        if ($category) {
            $subcategories = Category::query()->where('parent_id', $category->id)->get();
        }

        return view('front.products.index', compact('products', 'category', 'subcategories'));
    }

    public function categories2(Request $request, string $category, string $subCategory = null)
    {
        $searchText = trim($request->get('query'));
        $cid = explode('-', $category);

        if ($subCategory != null) {
            $subCategoryId = explode('-', $subCategory);
            $subCategory = Category::query()
                ->where(['id' => end($subCategoryId), 'is_published' => true])
                ->first();
        }

        $category = Category::query()
            ->where(['slug' => end($cid), 'is_published' => true])
            ->first();
        $subcategories = collect();

        $products = Product::query()->with(['media'])
            ->when($subCategory, function (Builder $query) use ($subCategory) {
                return $query->where('category_id', $subCategory->id);
            })
            ->when($category, function (Builder $query) use ($category) {
                return $query->whereIn('category_id', function ($query) use ($category) {
                    $query->select('id')->from('categories')
                        ->where('parent_id', $category->id)->orWhere('id', $category->id)
                        ->where('is_published', true);
                });
            })
            ->when($searchText && $searchText != '', function (Builder $query) use ($searchText) {
                return $query->where(function ($query) use ($searchText) {
                    return $query->where('title', 'like', "%{$searchText}%");
                });
            })
            ->when($request->filled('order_by'), function (Builder $query) use ($request) {
                list($field, $direction) = explode(".", $request->get('order_by'));
                return $query->orderBy($field, $direction);
            })
            ->when($request->filled('price_range'), function (Builder $query) use ($request) {
                list($min, $max) = explode(".", $request->get('price_range'));
                if ($min != null) {
                    $query->where('price', '>=', $min);
                }
                if ($max != null) {
                    $query->where('price', '<=', $max);
                }
            })
            ->when($request->filled('min_num') || $request->filled('max_num'), function (Builder $query) use ($request) {
                if ($request->get('min_num')) {
                    $query->where('price', '>=', $request->get('min_num'));
                }
                if ($request->get('max_num')) {
                    $query->where('price', '<=', $request->get('max_num'));
                }
            })
            ->when($request->filled('goals'), function (Builder $query) use ($request) {
                $i = 0;
                foreach ($request->goals as $gd) {
                    if ($i == 0) {
                        switch ($gd) {
                            case "Iloveyou":
                                $query->where('is_love', 1);
                                break;
                            case "congrotulation":
                                $query->where('is_congrotulation', 1);
                                break;
                            case "apoligize":
                                $query->where('is_apologize', 1);
                                break;
                            case "work":
                                $query->where('is_work', 1);
                                break;
                            case "birthday":
                                $query->where('is_birthday', 1);
                                break;
                            case "for_love":
                                $query->where('is_for_love', 1);
                                break;
                            case "be_sad":
                                $query->where('is_be_sad', 1);
                                break;
                            case "child":
                                $query->where('is_child', 1);
                                break;
                            case "anniversary":
                                $query->where('is_anniversary', 1);
                                break;
                            case "own":
                                $query->where('is_own', 1);
                                break;
                            case "opening":
                                $query->where('is_opening', 1);
                                break;
                            case "wedding":
                                $query->where('is_wedding', 1);
                                break;
                        }
                    } else {
                        switch ($gd) {
                            case "Iloveyou":
                                $query->orWhere('is_love', 1);
                                break;
                            case "congrotulation":
                                $query->orWhere('is_congrotulation', 1);
                                break;
                            case "apoligize":
                                $query->orWhere('is_apologize', 1);
                                break;
                            case "work":
                                $query->orWhere('is_work', 1);
                                break;
                            case "birthday":
                                $query->orWhere('is_birthday', 1);
                                break;
                            case "for_love":
                                $query->orWhere('is_for_love', 1);
                                break;
                            case "be_sad":
                                $query->orWhere('is_be_sad', 1);
                                break;
                            case "child":
                                $query->orWhere('is_child', 1);
                                break;
                            case "anniversary":
                                $query->orWhere('is_anniversary', 1);
                                break;
                            case "own":
                                $query->orWhere('is_own', 1);
                                break;
                            case "opening":
                                $query->orWhere('is_opening', 1);
                                break;
                            case "wedding":
                                $query->orWhere('is_wedding', 1);
                                break;
                        }
                    }
                    $i = 1;
                }
            })
            ->when($request->filled('category_list'), function (Builder $query) use ($request) {
                $query->whereIn('category_id', $request->category_list);
            })
            ->orderBy('is_featured');
        if ($request->filled('order_by')) {
            $products = $products->get();
        } else {
            $products = $products->orderByDesc('id')->get();
        }

        if ($request->ajax()) {
            return '';
            $renderedView = '';

            foreach ($products as $product) {
                $renderedView .= \view('front.products.product-card', ['product' => $product])->render();
            }

            return response()->json($renderedView, 200);
        }

        if ($category) {
            $subcategories = Category::query()->where('parent_id', $category->id)->get();
        }

        return view('front.products.index2', compact('products', 'category', 'subcategories'));
    }

    public function goal_categories(Request $request)
    {
        if (!$request->has('goals')) {
            return redirect()->route('front');
        }

        $products = Product::query()->with(['media'])
            ->when($request->filled('order_by'), function (Builder $query) use ($request) {
                list($field, $direction) = explode(".", $request->get('order_by'));
                return $query->orderBy($field, $direction);
            })
            ->when($request->filled('price_range'), function (Builder $query) use ($request) {
                list($min, $max) = explode(".", $request->get('price_range'));
                if ($min != null) {
                    $query->where('price', '>=', $min);
                }
                if ($max != null) {
                    $query->where('price', '<=', $max);
                }
            })
            ->when($request->filled('min_num') || $request->filled('max_num'), function (Builder $query) use ($request) {
                if ($request->get('min_num')) {
                    $query->where('price', '>=', $request->get('min_num'));
                }
                if ($request->get('max_num')) {
                    $query->where('price', '<=', $request->get('max_num'));
                }
            })
            ->when($request->filled('goals'), function (Builder $query) use ($request) {
                $i = 0;
                foreach ($request->goals as $gd) {
                    if ($i == 0) {
                        switch ($gd) {
                            case "Iloveyou":
                                $query->where('is_love', 1);
                                break;
                            case "congrotulation":
                                $query->where('is_congrotulation', 1);
                                break;
                            case "apoligize":
                                $query->where('is_apologize', 1);
                                break;
                            case "work":
                                $query->where('is_work', 1);
                                break;
                            case "birthday":
                                $query->where('is_birthday', 1);
                                break;
                            case "for_love":
                                $query->where('is_for_love', 1);
                                break;
                            case "be_sad":
                                $query->where('is_be_sad', 1);
                                break;
                            case "child":
                                $query->where('is_child', 1);
                                break;
                            case "anniversary":
                                $query->where('is_anniversary', 1);
                                break;
                            case "own":
                                $query->where('is_own', 1);
                                break;
                            case "opening":
                                $query->where('is_opening', 1);
                                break;
                            case "wedding":
                                $query->where('is_wedding', 1);
                                break;
                        }
                    } else {
                        switch ($gd) {
                            case "Iloveyou":
                                $query->orWhere('is_love', 1);
                                break;
                            case "congrotulation":
                                $query->orWhere('is_congrotulation', 1);
                                break;
                            case "apoligize":
                                $query->orWhere('is_apologize', 1);
                                break;
                            case "work":
                                $query->orWhere('is_work', 1);
                                break;
                            case "birthday":
                                $query->orWhere('is_birthday', 1);
                                break;
                            case "for_love":
                                $query->orWhere('is_for_love', 1);
                                break;
                            case "be_sad":
                                $query->orWhere('is_be_sad', 1);
                                break;
                            case "child":
                                $query->orWhere('is_child', 1);
                                break;
                            case "anniversary":
                                $query->orWhere('is_anniversary', 1);
                                break;
                            case "own":
                                $query->orWhere('is_own', 1);
                                break;
                            case "opening":
                                $query->orWhere('is_opening', 1);
                                break;
                            case "wedding":
                                $query->orWhere('is_wedding', 1);
                                break;
                        }
                    }
                    $i = 1;
                }
            })
            ->when($request->filled('category_list'), function (Builder $query) use ($request) {
                $query->whereIn('category_id', $request->category_list);
            })
            ->orderBy('is_featured');

        if ($request->filled('order_by')) {
            $products = $products->get();
        } else {
            $products = $products->orderByDesc('id')->get();
        }

        if ($request->ajax()) {
            return '';
            $renderedView = '';

            foreach ($products as $product) {
                $renderedView .= \view('front.products.product-card', ['product' => $product])->render();
            }

            return response()->json($renderedView, 200);
        }

        return view('front.products.goal', compact('products'));
    }
}
