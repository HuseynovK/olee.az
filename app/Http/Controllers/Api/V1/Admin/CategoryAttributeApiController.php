<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryAttributeRequest;
use App\Http\Requests\UpdateCategoryAttributeRequest;
use App\Http\Resources\Admin\CategoryAttributeResource;
use App\Models\CategoryAttribute;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryAttributeApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('category_attribute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CategoryAttributeResource(CategoryAttribute::with(['category'])->get());
    }

    public function store(StoreCategoryAttributeRequest $request)
    {
        $categoryAttribute = CategoryAttribute::create($request->all());

        return (new CategoryAttributeResource($categoryAttribute))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(CategoryAttribute $categoryAttribute)
    {
        abort_if(Gate::denies('category_attribute_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CategoryAttributeResource($categoryAttribute->load(['category']));
    }

    public function update(UpdateCategoryAttributeRequest $request, CategoryAttribute $categoryAttribute)
    {
        $categoryAttribute->update($request->all());

        return (new CategoryAttributeResource($categoryAttribute))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(CategoryAttribute $categoryAttribute)
    {
        abort_if(Gate::denies('category_attribute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categoryAttribute->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
