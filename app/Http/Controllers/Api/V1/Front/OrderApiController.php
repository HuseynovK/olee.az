<?php

namespace App\Http\Controllers\Api\V1\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\StoreOrderRequest;
use App\Http\Resources\Admin\OrderResource;
use App\Models\Address;
use App\Models\Cart;
use App\Models\City;
use App\Models\Coupon;
use App\Models\District;
use App\Models\Letter;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\Payment;
use App\Services\GoldenPay;
use Berkayk\OneSignal\OneSignalClient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class OrderApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new OrderResource(Order::with(['user', 'address'])->get());
    }

    public function show(Order $order)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new OrderResource($order->load(['user', 'address']));
    }

    public function store(StoreOrderRequest $request)
    {
        $district = null;
        $city = null;
        $address = null;
//dd($this->client->id);
        if (Auth::check()) {
            $request->validate(['address_id' => 'required'], [
                'address_id.required' => "Zəhmət olmasa Ünvan daxil edin",
            ]);
        } else {
            $request->validate([
                'buyer_firstname' => 'required',
                'buyer_tel' => 'required',
                'buyer_city_id' => 'required',
                'buyer_detail' => 'required',
            ], [
                'buyer_firstname.required' => "Zəhmət olmasa Ad Soyad bölməsin doldurun",
                'buyer_tel.required' => "Zəhmət olmasa Telefon bölməsin doldurun",
                'buyer_city_id.required' => "Zəhmət olmasa Ərazini seçin",
                'buyer_detail.required' => "Zəhmət olmasa Ətraflı ünvan bölməsin doldurun",
            ]);

            $dis = explode(',', $request->buyer_city_id);

            if (isset($dis[0])) {
                $city = City::where('id', $dis[0])->with('cityDistricts')->first();
                if (!$city) {
                    return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                }
                if (!$city->cityDistricts->isEmpty()) {
                    if (isset($dis[1])) {
                        if ($dis[1]) {
                            $district = District::where('id', $dis[1])->first();
                        } else {
                            return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                        }
                    } else {
                        return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                    }
                }
            } else {
                return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
            }

            $address = Address::query()->create([
                "full_name" => $request->input('buyer_firstname'),
                "phone" => $request->input('buyer_tel'),
                "city_id" => $city->id,
                "is_default" => $request->input('is_default') || $request->input('is_checkout'),
                "district_id" => $district ? $district->id : null,
                "detail" => $request->input('buyer_detail'),
                "title" => $request->get('title', 'Unnamed') ?? 'Unnamed',
                'user_id' => $this->client->getAttribute('id'),
            ]);
        }

        $user = $this->client;
        $cart = Cart::with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $user->getAttribute('id')]);


        if ($cart->cart_cart_items_count) {

            $first_order_discount = false;

            if (\auth()->check() && Order::query()->where('user_id', \auth()->id())->count() == 0) {
                $first_order_discount = true;
                $cart->setAttribute('payed', $cart->getAttribute('payed') * .95);
            }
//            $request->input('address_id')
            $addre2 = null;
            if (\auth()->check()) {
                $addre2 = Address::where('id', $request->input('address_id'))->first();
            } else {
                $addre2 = $address;
            }

            if ($addre2->district?->amount) {
                $cart->setAttribute('payed', $cart->getAttribute('payed') + $addre2->district->amount);
                $cart->setAttribute('total', $cart->getAttribute('total') + $addre2->district->amount);
            } elseif ($addre2->city?->amount) {
                $cart->setAttribute('payed', $cart->getAttribute('payed') + $addre2->city->amount);
                $cart->setAttribute('total', $cart->getAttribute('total') + $addre2->city->amount);
            }

            $order = Order::query()
                ->create([
                    'order_number' => rand(1000, 9999) . rand(1000, 9999),
                    'total' => $cart->total,
                    'payed' => $cart->payed,
                    'discount' => $cart->total - $cart->payed,
                    'is_gift' => $request->has('isGift'),
                    'is_urgent' => $request->has('isUrgent'),
                    'delivery_date' => $request->input('deliveryDate'),
                    'delivery_time' => $request->input('deliveryTime'),
                    'payment_type' => $request->input('paymentType'),
                    'sender' => [
                        'firstname' => $request->input('sender_firstname'),
                        'lastname' => $request->input('sender_lastname'),
                        'phone' => $request->input('sender_phone'),
                        'is_anonymous' => $request->input('sender_is_anonymous') ? "Yes" : "No",
                    ],
                    'note' => $request->input('note'),
                    'user_id' => $user != null ? $user->id : null,
                    'address_id' => Auth::check() ? $request->input('address_id') : $address->id,
                    'letter_id' => null,
//                    'letter_id' => $request->input('letter_id') ?? null,
                    'letter_message' => $request->input('letter_message'),
                    'coupon_id' => $cart->getAttribute('coupon_id'),
                    'first_order_discount' => $first_order_discount
                ]);

            if (session()->has('ref_id')) {
                $order->ref_id = session()->get('ref_id');
                $order->save();
            }

            if ($cart->getAttribute('coupon_id') != null) {
                Coupon::query()->where('id', $cart->getAttribute('coupon_id'))
                    ->update(['used_times' => DB::raw("(used_times + 1)")]);
            }

            $orderstatus = OrderStatus::orderBy('id', 'asc')->first();

            foreach ($cart->cartCartItems as $item) {
                $discount = 0;
                if ($item->discount_type == "percentage") {
                    $discount = $item->price * $item->discount * 0.01;
                } else {
                    $discount = $item->discount;
                }
                OrderDetail::create([
                    'total' => $item->price,
                    'payed' => $item->price - $discount,
                    'discount' => $discount,
                    'quantity' => $item->quantity,
                    'attributes' => $item->attributes,
                    'stock' => false,
                    'order_id' => $order->id,
                    'product_id' => $item->product_id,
                    'order_status_id' => $orderstatus?->id,
                ]);
//                $item->delete();
            }

            try {
                $oneSignalClient = new OneSignalClient(env('ONESIGNAL_APP_ID'), env('ONESIGNAL_REST_API_KEY'), null);
                $oneSignalClient->sendNotificationToAll(
                    "Yeni sifariş - {$cart->getAttribute('payed')} AZN.",
                    route('admin.orders.show', ['order' => $order->id]),
                    ['orders_count' => Order::query()->where('seen', false)->count()]);
            } catch (\Exception $exception) {
                Log::channel('onesignal')->error($exception->getMessage());
            }

            try {
                $this->smsService
                    ->send('994502281199', "Yeni sifariş NO - {$order->getAttribute('order_number')}. Məbləğ - {$cart->getAttribute('payed')}");
            } catch (\Exception $exception) {
                Log::channel('sms')->error($exception->getMessage());
            }

            if ($request->input('paymentType') == "cash") {
                $cart = Cart::with(['cartCartItems'])
                    ->withCount('cartCartItems')
                    ->where('user_id', $order->user_id)->delete();
                return response()->json(['success' => true, 'data' => $order, 'redirectUrl' => null]);
            } else {
                $amount = $cart->payed;
                if ($request->filled('letter_id')) {
                    $letter = Letter::query()->find($request->input('letter_id'));
                    $amount += $letter->price;
                }
                $amount = (float)round($amount, 2);
                $amount = $amount * 100;
                if ($request->input('isUrgent')) {
                    $amount += 500;
                }
                if ($request->input('sender_is_anonymous')) {
                    $amount += 500;
                }
                $resp = (new GoldenPay())->getPaymentKeyJSONRequest($amount, 'lv', 'v', $order->id);
                if ($resp->status->code == 1) {
                    Payment::create([
                        'key' => $resp->paymentKey,
                        'amount' => $amount,
                        'status' => 0,
                        'order_id' => $order->id,
                        'user_id' => $this->client->getAttribute('id'),
                    ]);
                    return response()->json(['success' => true, 'data' => $order, 'redirectUrl' => $resp->urlRedirect], Response::HTTP_ACCEPTED);
                }
                return response()->json(['success' => true, 'data' => $order, 'redirectUrl' => null], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        return response()->json(['success' => false, 'redirectUrl' => null], Response::HTTP_BAD_REQUEST);
    }

    public function redeemCoupon(Request $request)
    {
        $discount = 0;
        if ($request->filled('code')) {
            $coupon = Coupon::query()->where('code', $request->get('code'))->first();
            if ($coupon == null) {
                return response()->json(['errors' => ['code' => ['Kupon tapılmadı']]]);
            }
//            if ($coupon->coupon_type == 'count') {
//                if ($coupon->used_times >= $coupon->count) {
//                    return response()->json(['errors' => ['code' => ['Kuponun artiq istifadəsi bitmişdir']]]);
//                }
//            } elseif ($coupon->coupon_type == 'expire_date') {
//                if ($coupon->expire_date < Carbon::now()) {
//                }
//            }

            if (!$coupon->is_available) {
                return response()->json(['errors' => ['code' => ['Kuponun artiq istifadəsi bitmişdir']]]);
            }

            $cart = Cart::query()->where('user_id', $this->client->getAttribute('id'))->first();
            if ($cart) {

                if ($coupon->getAttribute('discount_type') == 'percentage') {
                    $discount = ($cart->getAttribute('payed') * $coupon->getAttribute('discount')) / 100;
                } else if ($coupon->getAttribute('discount_type') == 'flat') {
                    $discount = $coupon->getAttribute('discount');
                }

                $discount = (float)number_format($discount, 2, ',', '');

                $cart->setAttribute('coupon_id', $coupon->getAttribute('id'));
                $cart->setAttribute('payed', $cart->getAttribute('payed') - $discount);
                $cart->save();
//                dd("aasd");
                $coupon->update(['used_times' => $coupon->used_times + 1]);
//                dd("aasd");
            }
//            dd("aasd");
            return response()->json(['success' => true, 'data' => $coupon, 'final_discount' => $discount, 'cart' => $cart]);
        }
        return response()->json(['errors' => ['code' => ['Kuponu daxil edin!']]]);
    }
}
