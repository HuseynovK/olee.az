<?php

namespace App\Http\Controllers\Api\V1\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Http\Resources\Front\AddressResource;
use App\Models\Address;
use App\Models\City;
use App\Models\District;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AddressesApiController extends Controller
{
    public function store(StoreAddressRequest $request)
    {
        if ($request->input('is_default')) {
            Address::query()->where(['user_id' => $this->client->getAttribute('id')])->update(['is_default' => false]);
        }
        $item = Address::query()->create([
            "full_name" => $request->input('full_name'),
            "phone" => $request->input('phone'),
            "city_id" => $request->input('city_id'),
            "is_default" => $request->input('is_default') || $request->input('is_checkout'),
            "district_id" => $request->input('district_id'),
            "detail" => $request->input('detail'),
            "title" => $request->get('title', 'Unnamed') ?? 'Unnamed',
            'user_id' => $this->client->getAttribute('id'),
        ]);
        $item->distict_amount = $item->district?->amount;
        $item->city_amount = $item->city->amount;

        if ($request->input('is_checkout')) {
            $view = view('front.cart.address-card', ['address' => $item])->render();
            return response()->json(['success' => true, 'html' => $view, 'address_data' => $item], Response::HTTP_CREATED);
        } else {
            $view = view('front.user.partials.address-card', compact('item'))->render();
            return response()->json(['success' => true, 'html' => $view, 'address_data' => $item], Response::HTTP_CREATED);
        }
    }

    public function store_new(StoreAddressRequest $request)
    {
        $dis = explode(',', $request->district_id);
        $district = null;
        $city = null;
        if (isset($dis[0])) {
            $city = City::where('id', $dis[0])->with('cityDistricts')->first();
            if (!$city) {
                return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
            }
            if (!$city->cityDistricts->isEmpty()) {
                if (isset($dis[1])) {
                    if ($dis[1]) {
                        $district = District::where('id', $dis[1])->first();
                    } else {
                        return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                    }
                } else {
                    return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                }
            }
        } else {
            return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
        }

        if ($request->has('is_default')) {
//            if ($request->input('is_default') == 'on') {
            Address::query()->where(['user_id' => $this->client->getAttribute('id')])->update(['is_default' => false]);
//            }
        }
//        dd($request->all());
        $dist = District::where('id', $request->input('district_id'))->with('city')->first();
        $item = Address::query()->create([
            "full_name" => $request->input('full_name'),
            "phone" => $request->input('phone'),
            "city_id" => $city->id,
            "is_default" => $request->has('is_default') || $request->has('is_checkout'),
            "district_id" => $district ? $district->id : null,
            "detail" => $request->input('detail'),
            "title" => $request->get('title', 'Unnamed') ?? 'Unnamed',
            'user_id' => $this->client->getAttribute('id'),
        ]);
        $item->distict_amount = $dist->amount;
        $item->city_amount = $city->amount;

        if ($request->has('is_checkout')) {
            $view = view('front.cart.address-card', ['address' => $item])->render();
            return response()->json(['success' => true, 'html' => $view, 'address_data' => $item, 'neededd' => $item->is_default], Response::HTTP_CREATED);
        } else {
            $view = view('front.user.partials.address-card', compact('item'))->render();
            return response()->json(['success' => true, 'html' => $view, 'address_data' => $item, 'neededd' => $item->is_default], Response::HTTP_CREATED);
        }
    }

    public function update_new(Request $request)
    {
        $request->validate([
            'address_id' => [
                'required'
            ],
            'district_id' => [
                'required'
            ],
            'title' => [
                'nullable',
                'sometimes',
                'string',
                'min:2',
                'max:50',
            ],
            'detail' => [
                'string',
                'min:2',
                'max:255',
                'required',
            ],
            'full_name' => [
                'string',
                'min:2',
                'max:100',
                'required',
            ],
            'phone' => [
                'string',
                'min:7',
                'max:25',
                'required',
            ],
        ]);
        $item = Address::where('id', $request->address_id)->first();
        if (!$item) {
            return response()->json(['errors' => ['city' => ["Address duzgun secilmeyib!"]]]);
        }

        $dis = explode(',', $request->district_id);
        $district = null;
        $city = null;
        if (isset($dis[0])) {
            $city = City::where('id', $dis[0])->with('cityDistricts')->first();
            if (!$city) {
                return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
            }
            if (!$city->cityDistricts->isEmpty()) {
                if (isset($dis[1])) {
                    if ($dis[1]) {
                        $district = District::where('id', $dis[1])->first();
                    } else {
                        return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                    }
                } else {
                    return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                }
            }
        } else {
            return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
        }

        if ($request->has('is_default')) {
//            if ($request->input('is_default') == 'on') {
            Address::query()->where(['user_id' => $this->client->getAttribute('id')])->update(['is_default' => false]);
//            }
        }
//        $dist = District::where('id', $request->input('district_id'))->with('city')->first();
        $item->update([
            "full_name" => $request->input('full_name'),
            "phone" => $request->input('phone'),
            "city_id" => $city->id,
            "is_default" => $request->has('is_default') || $request->has('is_checkout'),
            "district_id" => $district ? $district->id : null,
            "detail" => $request->input('detail'),
            "title" => $request->get('title', 'Unnamed') ?? 'Unnamed',
            'user_id' => $this->client->getAttribute('id'),
        ]);
        $item->distict_amount = $district ? $district->amount : null;
        $item->city_amount = $city->amount;

        if ($request->input('is_checkout')) {
            $view = view('front.cart.address-card', ['address' => $item])->render();
            return response()->json(['success' => true, 'html' => $view, 'address_data' => $item, 'neededd' => $item->is_default], Response::HTTP_CREATED);
        } else {
            $view = view('front.user.partials.address-card', compact('item'))->render();
            return response()->json(['success' => true, 'html' => $view, 'address_data' => $item, 'neededd' => $item->is_default], Response::HTTP_CREATED);
        }
    }

    public function setIsDefaultAddress(Request $request)
    {
//        dd($request);
        $request->validate([
            'id' => 'required'
        ]);
        Address::where('id', $request->id)->update([
            'is_default' => 1
        ]);
        Address::where('id', '!=', $request->id)->update([
            'is_default' => 0
        ]);
    }

    public function update(UpdateAddressRequest $request, Address $address)
    {
        $address->update($request->all());
        return (new AddressResource($address))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function update_client(\App\Http\Requests\Front\UpdateAddressRequest $request, Address $address)
    {
        $address->update($request->all());
        return \response()->json(['success' => true]);
    }

    public function destroy(Address $address)
    {
        $addd = null;
        if ($address->getAttribute('user_id') == $this->client->getAttribute('id')) {
            if ($address->getAttribute('is_default')) {
                $addd = Address::query()
                    ->where(['user_id' => $this->client->getAttribute('id')])
                    ->with(['city', 'district'])
                    ->first()
                    ->update(['is_default' => true]);
            }
            $address->delete();
            return response()->json(['data' => $addd]);
        }
        return response()->json(['data' => $addd]);
    }

    public function default(Address $address)
    {
        if ($address->getAttribute('user_id') == $this->client->getAttribute('id')) {
            Address::query()
                ->where(['user_id' => $this->client->getAttribute('id')])
                ->update(['is_default' => false]);
            $address->update(['is_default' => true]);
            return response(null, Response::HTTP_NO_CONTENT);
        }
        return response(null, Response::HTTP_NOT_FOUND);
    }
}
