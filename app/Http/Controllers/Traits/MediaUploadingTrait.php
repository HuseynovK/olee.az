<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;

trait MediaUploadingTrait
{
    public function storeMedia(Request $request)
    {
// Validates file size
        if ($request->filled('size')) {
            $request->validate([
                'file' => 'max:' . $request->get('size', 50) * 1024,
            ]);
        }

// If width or height is preset - we are validating it as an image
        if ($request->filled('width') || $request->filled('height')) {
            $request->validate([
                'file' => sprintf(
                    'image|dimensions:max_width=%s,max_height=%s',
                    $request->get('width', 100000),
                    $request->get('height', 100000)
                ),
            ]);
        }

        $path = storage_path('tmp/uploads');

        try {
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
        } catch (\Exception $e) {
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
}
