<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\StoreContactMessageRequest;
use App\Models\ContactMessage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\ValidationException;

class ContactMessageController extends Controller
{
    public function index()
    {
        return view('front.contact.index');
    }

    public function store(StoreContactMessageRequest $request)
    {
        try {
            ContactMessage::create($request->only(['firstname', 'lastname', 'phone', 'subject', 'message']));
            return response()->json(['success' => true, 'message' => translate('successfully sent'), 'data' => null], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            throw ValidationException::withMessages(['error' => $e->getMessage()]);
        }
    }
}
