<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\LoginRequest;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */
//    use Authenticatable;
//    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        parent::__construct();
//        //        $this->middleware('guest')->except('logout');
//    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('front');
    }

    public function login_index()
    {
        return view('front.auth.login');
    }

    public function login(Request $request)
    {
//        dd(bcrypt("123456"));
        if ($request->filled('phone')) {
            $phone = $this->preprocessNumber($request->get('phone'));
            $request->merge(['phone' => $phone]);
        }

        $request->validate([
            'phone' => 'required|exists:users,phone',
            'password' => 'required',
        ], [
            'phone.required' => "Zəhmət olmasa, telefon nömrəni daxil edin",
            'password.required' => "Zəhmət olmasa, şifrəni daxil edin",
            'phone.exists' => "Bu nömrə qeydiyyatdan kecmeyib və ya nömrə təsdiqlənməyib",
        ]);

        $user = User::query()->with('userCart.cartCartItems')
            ->whereNull('deleted_at')
            ->where('phone', 'like', "%{$request->get('phone')}")->first();
        Log::info("Login Started");
        if ($user && Hash::check($request->get('password'), $user->password)) {
            if ($user->phone_verified_at != null) {
                $client = $this->client;
                Log::info("Session Cart:");
                $cart = Cart::with(['cartCartItems'])
                    ->firstOrCreate(['user_id' => $client->getAttribute('id')]);
//                dd("KAmil");
                Log::info($cart);
                Log::info("User Cart:");
                $user_cart = Cart::with(['cartCartItems'])->orderByDesc('id')->where('user_id', $user->getAttribute('id'))
                    ->first();
                Log::info($user_cart);
                Auth::loginUsingId($user->getAttribute('id'));
//                $cart->update(['user_id' => $user->id]);
//                dd($user_cart->cartCartItems);
                if ($cart && !$user_cart) {
                    Log::info("111111");
                    $cart->update(['user_id' => $user->id]);
                } else if ($cart && $user_cart) {
                    Log::info("@22222");
//                    if ($user_cart->cartCartItems->isNotEmpty()) {
                        Log::info("@3333333");
                        $total = 0;
                        $price = 0;
                        foreach ($cart->cartCartItems as $cartCartItem) {
                            $total += $cartCartItem->price;
                            if ($cartCartItem->discount) {
                                if ($cartCartItem->discount_type == 'flat') {
                                    $price += ($cartCartItem->price - $cartCartItem->discount);
                                } else if ($cartCartItem->discount_type == 'percentage') {
                                    $price += ($cartCartItem->price * $cartCartItem->discount * 0.01);
                                }
                            } else {
                                $price += $cartCartItem->price;
                            }
                            $cartCartItem->update([
                                'cart_id' => $user_cart->id
                            ]);
                        }
                        $user_cart->update([
                            'total' => $user_cart->total + $total,
                            'payed' => $user_cart->payed + $price,
                        ]);
                        $user_cart->refresh();
                        $cart->delete();
//                    } else {
//                        Log::info("44444444444");
//                        $cart->update(['user_id' => $user->id]);
//                    }
                }
                User::query()
                    ->where('session', session('basket_token', 'asdfg'))
                    ->delete();
                session()->remove('basket_token');
                return response()->json(['success' => true]);
            } else {
                Auth::logout();
                return response()->json(['errors' => ['phone' => ['Zəhmət olmasa Mobile nomreni tesdiqleyin!']]]);
            }
        } else {
            return response()->json(['errors' => ['phone' => ['Password sehvdir!']]]);
        }
    }
}
