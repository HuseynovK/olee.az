<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Models\Sms;
use App\Models\User;
use App\Notifications\VerifyUserNotification;
use App\Providers\RouteServiceProvider;
use App\Services\IServices\ISmsService;
use App\Services\ServiceResources\ServiceSmsRecipient;
use App\Services\SmsService;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class   RegisterController extends Controller
{
    // use AuthenticatesUsers;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    //use RegistersUsers;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function register_index()
    {
        return view('front.auth.register');
    }

    public function register(Request $request)
    {
        if ($request->filled('phone')) {
            $phone = $this->preprocessNumber($request->get('phone'));
            $request->merge(['phone' => $phone]);
        }
        $request->validate([
            'phone' => ['required', Rule::unique('users', 'phone')
                ->whereNotNull('phone_verified_at')
                ->whereNull('deleted_at')]
        ], [
            'phone.required' => "Zəhmət olmasa, telefon nömrəni daxil edin",
            'phone.unique' => 'Bu nömrə artıq qeydiyyatdan keçmişdir.',
        ]);
        $token = rand(1000, 999999);
        $user = User::query()->updateOrCreate(
            [
                'phone' => $request->input('phone')
            ],
            [
                'verification_token' => bcrypt($token),
            ]);


        $message = "Hesabinizi tesdiqlemek uchun kod: " . $token;
        $sms = $this->smsService->send($user->getAttribute('phone'), $message);

        $responseBody = simplexml_load_string($sms->getAttribute('response'));
        Log::info("Register send message :");
        Log::info($sms);
        Log::info($sms->response);
//        Log::info($sms->response->head);
//        Log::info($sms->response->responsecode);
        Log::info($responseBody);
//        dd($sms,$responseBody);
        if ($sms->status == SmsService::StatusSuccess || $responseBody->head->responsecode == "000") {
            return response()->json(['url' => route('user.password-reset.get') . "?phone=" . $request->phone]);
        }

        return response()->json(['errors' => ['phone' => ['Something went wrong!']]]);
    }

    public function setPassword(Request $request)
    {
        $request->validate([
            'phone' => ['required', 'exists:users,phone'],
            'code' => ['required'],
            'new_password' => 'required',
            'new_confirm_password' => 'required|same:new_password'
        ], [
            'phone.required' => "Zəhmət olmasa, telefon nömrəni daxil edin",
            'code.required' => "Zəhmət olmasa, kodu daxil edin",
            'new_password.required' => "Zəhmət olmasa, şifrəni daxil edin",
            'new_confirm_password.required' => "Şifrələr uyuşmur",
            'phone.exists' => 'Bu nömrə qeydiyyatdan keçməmişdir.',
        ]);
        $user = User::where([
            'phone' => $request->input('phone')
        ])->first();
        if ($user) {
            if (Hash::check($request->code, $user->verification_token)) {
                $user->password = $request->input('new_password');
                $user->save();
                Auth::login($user);
                return response()->json(['url' => route('front')]);
            }
        }
        return response()->json(['errors' => ['phone' => ['Istifadəçi tapılmadı']]]);
    }

    public function setPassword_get(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'phone' => 'required'
        ]);
        return view('front.auth.setPassword_register')->with(['phone' => $request->phone, 'code' => $request->code]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
}
