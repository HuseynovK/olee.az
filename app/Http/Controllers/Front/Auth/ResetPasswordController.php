<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    // use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $request)
    {
        $request->validate([
            'phone' => ['required', 'exists:users,phone'],
            'code' => ['required', 'exists:users,verification_token'],
            'password' => ['required', 'min:5'],
        ]);
        $user = User::query()->where([
            'phone' => $request->input('phone'),
            'verification_token' => $request->input('code'),
        ])->first();

        if ($user) {
            $user->password = $request->input('password');
            $user->save();
            return response()->json(['success' => true, 'data' => $user, 'errors' => null]);
        } else {
            throw ValidationException::withMessages(['code' => trans('password.token')]);
        }
    }

    public function reset_get(Request $request)
    {
        if (!$request->has('phone')) {
            abort(404);
        }
        return view('front.auth.forgot_confirm-code')->with('phone', $request->phone);
    }
}
