<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Models\Sms;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;

    public function forgotPass(Request $request)
    {
        return view('front.auth.forgot-password');
    }

    public function sendToken(Request $request)
    {
        if ($request->filled('phone')) {
            $phone = $this->preprocessNumber($request->get('phone'));
            $request->merge(['phone' => $phone]);
        }
        $request->validate([
            'phone' => ['required', Rule::exists('users', 'phone')->whereNull('deleted_at')->where('verified', true)],
        ], [
            'phone.required' => "Zəhmət olmasa, telefon nömrəni daxil edin",
            'phone.exists' => "Bu nömrə qeydiyyatdan kecmeyib və ya nömrə təsdiqlənməyib",
        ]);

        $user = User::where(['phone' => $request->input('phone')])->where('verified', true)->firstOrFail();
        $token = rand(1000, 999999);
        $user->verification_token = Hash::make($token);
        $user->save();

        $message = "Shifrenizi yenilemek uchun kod: " . $token;

        $sms = $this->smsService->send($user->getAttribute('phone'), $message);

        $responseBody = $sms->getAttribute('response');

        $responseBody = simplexml_load_string($responseBody);
        Log::info("Forgot send message :");
        Log::info($responseBody);
        if ($responseBody->head->responsecode == "000") {
            return response()->json(['url' => route('user.password-reset.get') . "?phone=" . $request->phone]);
        }

        return response()->json(['errors' => ['phone' => ['Something went wrong']]]);
    }
}
