<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\SmsService;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    //use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        //$this->middleware('signed')->only('verify');
        parent::__construct();
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verify(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'phone' => 'required'
        ]);
        $token = $request->input('code');
        $user = User::where(['phone' => $request->input('phone')])->first();
        if ($user) {
            if (Hash::check($request->code, $user->verification_token)) {
                $user->phone_verified_at = now();
                $user->verified_at = now();
                $user->verified = true;
                $user->save();
                return response()->json(['url' => route('user.register.setpassword.get') . '?phone=' . $user->phone . '&code=' . $request->code]);
            }
            return response()->json(['errors' => ['phone' => ['Kod duz deyil!']]]);
        }
        return response()->json(['errors' => ['phone' => ['Kod duz deyil']]]);
    }

    public function resend(Request $request)
    {
        $request->validate([
            'phone' => ['required', Rule::unique('users', 'phone')
                ->whereNotNull('phone_verified_at')
                ->whereNull('deleted_at')]
        ]);
        $user = User::where('phone', $request->phone)->first();

        $token = rand(1000, 999999);
        $user->update(
            [
                'verification_token' => bcrypt($token),
            ]);


        $message = "Hesabinizi tesdiqlemek uchun kod: " . $token;
        $sms = $this->smsService->send($user->getAttribute('phone'), $message);

        $responseBody = simplexml_load_string($sms->getAttribute('response'));
        Log::info("Resend Verification send message :");
        Log::info($responseBody);
        if ($sms->status == SmsService::StatusSuccess || $responseBody->head->responsecode == "000") {
            return redirect()->back();
        }
        return redirect()->back()->withErrors(['code' => "Something went wrong"]);
    }

    public function check(Request $request)
    {
        $token = $request->input('code');
        $user = User::where(['phone' => $request->input('phone'), 'verification_token' => $token])->first();
        if ($user) {
            return response()->json(['success' => true, 'token' => $token, 'errors' => null]);
        }

        throw ValidationException::withMessages(['code' => "Kod düzgün daxil edilməyib"]);
    }
}
