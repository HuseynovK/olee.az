<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Payment;
use App\Services\GoldenPay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller
{
    public function check(Request $request)
    {
        $resp = (new GoldenPay())->getPaymentResult($request->get('payment_key'));
        //return response()->json($resp);
        if ($resp->status->code == 1) {
            $payment = Payment::where(['key' => $request->get('payment_key')])->firstOrFail();
            $order = Order::where(['id' => $payment->order_id])->firstOrFail();
            $payment->update(['status' => true]);
            $order->update(['payment_status' => true]);
            $cart = Cart::with(['cartCartItems'])
                ->withCount('cartCartItems')
                ->where('user_id', $order->user_id)->delete();
            return Redirect::to(route('front.checksuc') . "?order_number=" . $order->order_number);
        }
        return Redirect::to(route('front.checkerror') . "?order_number=");
    }
}
