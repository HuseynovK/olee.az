<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\StoreOrderRequest;
use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\City;
use App\Models\District;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Services\Cart as ServicesCart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {

    }

    public function status(Request $request)
    {
        $order = Order::query()->with(['statuses'])
            ->where('order_number', $request->get('order_number'))->first();
        return view("front.orders.status")->with(compact('order'));
    }

    public function BuyOneProduct(Request $request)
    {
//        print_r($request);
        //dd($request->all());
        $request->validate([
            'product_id' => 'required',
            'full_name' => 'required',
            'phone' => 'required',
            'detail' => 'required',
        ]);

        $dis = explode(',', $request->district_id);
        $district = null;
        $city = null;
        if (isset($dis[0])) {
            $city = City::where('id', $dis[0])->with('cityDistricts')->first();
            if (!$city) {
                return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
            }
            if (!$city->cityDistricts->isEmpty()) {
                if (isset($dis[1])) {
                    if ($dis[1]) {
                        $district = District::where('id', $dis[1])->first();
                    } else {
                        return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir"]]]);
                    }
                } else {
                    return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
                }
            }
        } else {
            return response()->json(['errors' => ['city' => ["Ərazi mütləq seçilməlidir!"]]]);
        }
        $dist = District::where('id', $request->input('district_id'))->with('city')->first();
        $address = Address::query()->create([
            "full_name" => $request->input('full_name'),
            "phone" => $request->input('phone'),
            "city_id" => $city->id,
            "district_id" => $district ? $district->id : null,
            "detail" => $request->input('detail'),
            "title" => $request->get('title', 'Unnamed'),
            'user_id' => $this->client->getAttribute('id'),
        ]);
        $address->distict_amount = $dist->amount;
        $address->city_amount = $city->amount;

//        $cart = Cart::qury()->with(['cartCartItems'])
//            ->withCount('cartCartItems')
//            ->firstOrCreeate(['user_id' => $this->client->getAttribute('id')]);
        $product = Product::with([
            'productProductAttributes',
            'colors',
            'productProductVariations',
            'category.parent.categoryCategoryAttributes' => function ($query) {
                return $query->first();
            }
        ])->findOrFail($request->input('product_id'));

        $price = (new ServicesCart())->calculate($request->input('product_id'), $request);
        $catdirilma_amount = $address->distict_amount ? $address->distict_amount : $address->city_amount;
        $total = $price['net'] + $catdirilma_amount;
        $payed = $price['payable'] + $catdirilma_amount;

        $attributes = array();
        if ($product->has('productProductVariations') && $request->input('product_variation')) {
            $pV = $product->productProductVariations()->find($request->input('product_variation'));
            $attributes['variation'][0]['name'] = $product->category->parent?->categoryCategoryAttributes?->first()?->title ?? $product->category?->categoryCategoryAttributes?->first()?->title;
//            $attributes['variation'][0]['name'] = "HEcm";
            $attributes['variation'][0]['detail']['id'] = $pV->id;
            $attributes['variation'][0]['detail']['title'] = $pV->title . ' ' . $pV->description;
            $attributes['variation'][0]['detail']['price'] = $pV->price;
            $attributes['variation'][0]['detail']['discount'] = $pV->discount;
        }

        if ($product->has('colors') && $request->input('color')) {
            $color = $product->colors()->find($request->input('color'));
            $attributes['variation'][1]['name'] = translate('color');
            $attributes['variation'][1]['detail']['id'] = $color->id;
            $attributes['variation'][1]['detail']['title'] = $color->title;
            $attributes['variation'][1]['detail']['price'] = 0;
            $attributes['variation'][1]['detail']['discount'] = 0;
        }

        $k = 0;
        if ($product->has('productProductAttributes') && $request->input('attributeValue')) {
            foreach ($request->input('attributeValue') as $key => $attribute) {
                foreach ($attribute as $kk => $item) {
                    $_attribute = ProductAttribute::query()
                        ->where(['id' => $kk])->first();
                    if ($key == 'select') {
                        $_attributeValue = ProductAttributeValue::query()
                            ->where(['id' => $item, 'product_attribute_id' => $kk])->first();
                        $attributes['attribute'][$k]['name'] = $_attribute->title;
                        $attributes['attribute'][$k]['detail']['id'] = $_attributeValue->id;
                        $attributes['attribute'][$k]['detail']['title'] = $_attributeValue->title;
                        $attributes['attribute'][$k]['detail']['price'] = $_attributeValue->price;
                        $attributes['attribute'][$k++]['detail']['discount'] = 0;
                    }

                    if ($key == 'input' && ($item != null && $item != '')) {
                        $attributes['attribute'][$k]['name'] = $_attribute->title;
                        $attributes['attribute'][$k]['detail']['id'] = $_attribute->id;
                        $attributes['attribute'][$k]['detail']['title'] = $item;
                        $attributes['attribute'][$k]['detail']['price'] = $_attribute->price;
                        $attributes['attribute'][$k++]['detail']['discount'] = 0;
                    }
                }
            }
        }

        if ($request->file('attributeValue')) {
            $file2 = $request->file('attributeValue')['file'];
            $name = null;
            foreach ($file2 as $kk => $item) {
//                dd();
                $_attribute = ProductAttribute::query()
                    ->where(['id' => $kk])->first();
                $name = time() . '_' . $item->getClientOriginalName();
                $attributes['attribute'][$k]['name'] = $_attribute->title;
                $attributes['attribute'][$k]['detail']['id'] = $_attribute->id;
                $attributes['attribute'][$k]['detail']['title'] = $item->getClientOriginalName();
                $attributes['attribute'][$k]['detail']['image_name'] = $name;
                $attributes['attribute'][$k]['detail']['price'] = $_attribute->price;
                $attributes['attribute'][$k++]['detail']['discount'] = 0;
                Storage::disk('local')->putFileAs('public/attirbut_images/', $item, $name);
            }
        }
        $order = Order::query()
            ->create([
                'order_number' => rand(1000, 9999) . rand(1000, 9999),
                'total' => $total,
                'payed' => $payed,
                'discount' => $total - $payed,
                'payment_type' => 'cash',
                'user_id' => $this->client->id,
                'address_id' => $address->id,
            ]);
//        try {
            $orddd = OrderDetail::query()->create([
                'total' => $order->total,
                'payed' => $order->payed,
                'discount' => $order->discount,
                'quantity' => 1,
                'attributes' => json_encode($attributes),
                'stock' => false,
                'order_id' => $order->id,
                'product_id' => $product->getAttribute('id'),
                'order_status_id' => 1,
            ]);
//        }catch (\Exception $exception){
//            dd($exception);
//        }

        return \response()->json(['url'=>route('front.checksuc') . "?order_number=" . $order->order_number]);
    }
}
