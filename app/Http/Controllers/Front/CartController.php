<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\City;
use App\Models\Color;
use App\Models\Coupon;
use App\Models\District;
use App\Models\Letter;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\User;
use App\Models\Variation;
use App\Services\Cart as ServicesCart;
use App\Traits\ApiResponser;
use Carbon\CarbonImmutable;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CartController extends Controller
{
    public function show()
    {
        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);
        $related = null;

        if ($cart) {
            $cartProducts = $cart->cartCartItems->pluck('product_id')->toArray();
            $related = Product::query()->whereNotIn('id', $cartProducts)
                ->inRandomOrder()->limit(6)->get();

            $cart->calculate();
        }
        return view('front.cart.index2', compact('cart', 'related'));
    }

    public function list()
    {
        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);

        $cart->calculate();

        $html = view('front.cart.cart-card', compact('cart'))->render();
        return response()->json(['html' => $html, 'cart' => $cart]);
    }


    /**
     * Add to cart
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => ['required', 'exists:products,id']
        ]);

        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);

        $product = Product::with([
            'productProductAttributes',
            'colors',
            'productProductVariations',
            'category.parent.categoryCategoryAttributes' => function ($query) {
                return $query->first();
            }
        ])->findOrFail($request->input('product_id'));

        $price = (new ServicesCart())->calculate($request->input('product_id'), $request);

        $cart->update([
            'total' => $cart['total'] + $price['net'],
            'payed' => $cart['payed'] + $price['payable'],
        ]);

        $attributes = array();
        if ($product->has('productProductVariations') && $request->input('product_variation')) {
            $pV = $product->productProductVariations()->find($request->input('product_variation'));
            $attributes['variation'][0]['name'] = $product->category->parent?->categoryCategoryAttributes?->first()?->title ?? $product->category?->categoryCategoryAttributes?->first()?->title;
//            $attributes['variation'][0]['name'] = "HEcm";
            $attributes['variation'][0]['detail']['id'] = $pV->id;
            $attributes['variation'][0]['detail']['title'] = $pV->title . ' ' . $pV->description;
            $attributes['variation'][0]['detail']['price'] = $pV->price;
            $attributes['variation'][0]['detail']['discount'] = $pV->discount;
        }

        if ($product->has('colors') && $request->input('color')) {
            $color = $product->colors()->find($request->input('color'));
            $attributes['variation'][1]['name'] = translate('color');
            $attributes['variation'][1]['detail']['id'] = $color->id;
            $attributes['variation'][1]['detail']['title'] = $color->title;
            $attributes['variation'][1]['detail']['price'] = 0;
            $attributes['variation'][1]['detail']['discount'] = 0;
        }

        $k = 0;
        if ($product->has('productProductAttributes') && $request->input('attributeValue')) {
            foreach ($request->input('attributeValue') as $key => $attribute) {
                foreach ($attribute as $kk => $item) {
                    $_attribute = ProductAttribute::query()
                        ->where(['id' => $kk])->first();
                    if ($key == 'select') {
                        $_attributeValue = ProductAttributeValue::query()
                            ->where(['id' => $item, 'product_attribute_id' => $kk])->first();
                        $attributes['attribute'][$k]['name'] = $_attribute->title;
                        $attributes['attribute'][$k]['detail']['id'] = $_attributeValue->id;
                        $attributes['attribute'][$k]['detail']['title'] = $_attributeValue->title;
                        $attributes['attribute'][$k]['detail']['price'] = $_attributeValue->price;
                        $attributes['attribute'][$k++]['detail']['discount'] = 0;
                    }

                    if ($key == 'input' && ($item != null && $item != '')) {
                        $attributes['attribute'][$k]['name'] = $_attribute->title;
                        $attributes['attribute'][$k]['detail']['id'] = $_attribute->id;
                        $attributes['attribute'][$k]['detail']['title'] = $item;
                        $attributes['attribute'][$k]['detail']['price'] = $_attribute->price;
                        $attributes['attribute'][$k++]['detail']['discount'] = 0;
                    }

//                    if ($key == 'file' && ($item != null && $item != '')) {
//                        $attributes['attribute'][$k]['name'] = $_attribute->title;
//                        $attributes['attribute'][$k]['detail']['id'] = $_attribute->id;
//                        $attributes['attribute'][$k]['detail']['title'] = $name;
//                        $attributes['attribute'][$k]['detail']['image_name'] = $name;
//                        $attributes['attribute'][$k]['detail']['price'] = $_attribute->price;
//                        $attributes['attribute'][$k++]['detail']['discount'] = 0;
//                    }
                }
            }
        }

        if ($request->file('attributeValue')) {
            $file2 = $request->file('attributeValue')['file'];
            $name = null;
            foreach ($file2 as $kk => $item) {
//                dd();
                $_attribute = ProductAttribute::query()
                    ->where(['id' => $kk])->first();
                $name = time() . '_' . $item->getClientOriginalName();
                $attributes['attribute'][$k]['name'] = $_attribute->title;
                $attributes['attribute'][$k]['detail']['id'] = $_attribute->id;
                $attributes['attribute'][$k]['detail']['title'] = $item->getClientOriginalName();
                $attributes['attribute'][$k]['detail']['image_name'] = $name;
                $attributes['attribute'][$k]['detail']['price'] = $_attribute->price;
                $attributes['attribute'][$k++]['detail']['discount'] = 0;
                Storage::disk('local')->putFileAs('public/attirbut_images/', $item, $name);
            }
        }
//        dd($price['net']);
        //return md5(json_encode($attributes));
        //return Hash::check(json_encode($attributes), '$2y$10$QuovilQr3fTiLF4HDPz6SuJse7dRTwiQ.dNFkpuLcS1b1i7bJpScS') ? "true" : "false";
        //return Hash::make(json_encode($attributes));

        CartItem::query()->updateOrCreate([
            'cart_id' => $cart->getAttribute('id'),
            'product_id' => $product->getAttribute('id'),
            'sku' => md5(json_encode($attributes)),
        ], [
            'price' => $price['net'],
            'discount' => $price['net'] - $price['payable'],
            'discount_type' => 'flat',
            'quantity' => DB::raw('quantity + 1'),
            'attributes' => json_encode($attributes),
        ]);

        $cart->refresh();
        return response()->json(['data' => $cart]);
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);

        $cartItem = $cart->cartCartItems()->firstWhere('id', $id);
        if ($cartItem) {
            $payable = $cartItem->price;
            if ($cartItem->discount && $cartItem->discount_type == 'flat') {
                $payable = $cartItem->price - $cartItem->discount;
            }
            if ($cartItem->discount && $cartItem->discount_type == 'percentage') {
                $payable = $cartItem->price - $cartItem->price * $cartItem->discount * 0.01;
            }

            $cart->update([
                'total' => $cart->total - $cartItem->price * $cartItem->quantity,
                'payed' => $cart->payed - $payable * $cartItem->quantity,
            ]);

            $cartItem->delete();
            $cart->refresh();
            $cart->loadCount('cartCartItems');
            if ($cart->cartCartItems->count() < 1) {
                $cart->delete();

                return response()->json(['success' => true, 'data' => $cart, 'message' => null]);
            }

            return response()->json(['success' => true, 'data' => $cart, 'message' => null]);
        }
        return response()->json(['success' => false, 'data' => null, 'message' => translate('item not found')], 404);
    }

    /**
     * Increment cart item quantity
     *
     * @param int $id
     * @return JsonResponse
     */
    public function increase(int $id): JsonResponse
    {
        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);

        $cartItem = $cart->cartCartItems->firstWhere('id', $id);
        if ($cartItem) {
            $payable = $cartItem->price;

            if ($cartItem->discount && $cartItem->discount_type == 'flat') {
                $payable = $cartItem->price - $cartItem->discount;
            }
            if ($cartItem->discount && $cartItem->discount_type == 'percentage') {
                $payable = $cartItem->price - $cartItem->price * $cartItem->discount * 0.01;
            }

            $cart->update([
                'total' => $cart->total + $cartItem->price,
                'payed' => $cart->payed + $payable,
            ]);

            $cartItem->update(['quantity' => DB::raw('quantity + 1')]);

            $cart->refresh();

            return response()->json(['success' => true, 'data' => $cart, 'message' => null], 200);
        }
        return response()->json(['success' => false, 'data' => null, 'message' => translate('item not found')], 404);
    }

    public function omChangeAmountCart(int $id, Request $request): JsonResponse
    {
        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);

        $cartItem = $cart->cartCartItems->firstWhere('id', $id);
        if ($cartItem) {
            $payable = $cartItem->price;

            if ($cartItem->discount && $cartItem->discount_type == 'flat') {
                $payable = $cartItem->price - $cartItem->discount;
            }
            if ($cartItem->discount && $cartItem->discount_type == 'percentage') {
                $payable = $cartItem->price - $cartItem->price * $cartItem->discount * 0.01;
            }

            $cart->update([
                'total' => $cart->total + $cartItem->price,
                'payed' => $cart->payed + $payable,
            ]);

            $cartItem->update(['quantity' => $request->quantity]);

            $cart->refresh();

            return response()->json(['success' => true, 'data' => $cart, 'message' => null], 200);
        }
        return response()->json(['success' => false, 'data' => null, 'message' => translate('item not found')], 404);
    }

    /**
     * Decrement cart item quantity
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function decrease(int $id): JsonResponse
    {
        $cart = Cart::query()->with(['cartCartItems'])
            ->withCount('cartCartItems')
            ->firstOrCreate(['user_id' => $this->client->getAttribute('id')]);

        $cartItem = $cart->cartCartItems->firstWhere('id', $id);

        if ($cartItem) {

            $payable = $cartItem->price;
            if ($cartItem->discount && $cartItem->discount_type == 'flat') {
                $payable = $cartItem->price - $cartItem->discount;
            }
            if ($cartItem->discount && $cartItem->discount_type == 'percentage') {
                $payable = $cartItem->price - $cartItem->price * $cartItem->discount * 0.01;
            }

            $cart->update([
                'total' => $cart->total - $cartItem->price,
                'payed' => $cart->payed - $payable,
            ]);

            if ($cartItem->quantity > 1) {
                $cartItem->update(['quantity' => DB::raw('quantity - 1')]);
            } else {
                $cartItem->delete();
            }
            $cart->refresh();

            return response()->json(['success' => true, 'data' => $cart, 'message' => null], 200);
        }

        return response()->json(['success' => false, 'data' => null, 'message' => translate('item not found')], 404);
    }


    /**
     * Cart checkout
     * @return View|RedirectResponse
     */
    public function checkout2(Request $request): View|RedirectResponse
    {
        $user = $this->client;
        if ($user) {
            $cart = Cart::query()->with(['cartCartItems', 'coupon'])
                ->withCount('cartCartItems')
                ->where(['user_id' => $user->getAttribute('id')])
                ->first();

            if (!$cart) {
                return redirect()->route('front');
            }
            $cart->calculate();

            $cities = null;
            $districts = null;
            $prepTime = null;
            $letters = null;
            $addresses = null;
            $special_discount = null;
            if (!$cart && !session()->has('payment')) {
                return redirect()->route('front');
            }

            if (session()->has('payment')) {
                $with = ['payment' => session()->get('payment'), 'order' => session()->get('order')];
                return view('front.cart.checkout', compact('cart', 'user', 'cities', 'districts', 'prepTime', 'letters', 'addresses', 'special_discount'))->with($with);
            }
        } else {
            return redirect()->route('front');
        }

        $cities = City::all();
        $districts = District::all();
        $currentDate = CarbonImmutable::now();
        $prepTime = null;
        $tecili_tarix = 0;
//        dd($cart->cartCartItems);
        foreach ($cart->cartCartItems as $key => $item) {
            if ($item->product->preperation_time) {

                if ($item->product->preperation_time_unit == 'days') {
                    $tecili_tarix += 24 * $item->product->preperation_time * $item->quantity;
                    $deliveryDate = $currentDate->addDays($item->product->preperation_time);
                } elseif ($item->product->preperation_time_unit == 'hours') {
//                    dd("KAmil");
                    $tecili_tarix += ($item->product->preperation_time * $item->quantity);
                    $deliveryDate = $currentDate->addHours($item->product->preperation_time);
                } else {
                    $deliveryDate = $currentDate;
                }

                if ($tecili_tarix > 2) {
                    $prepTime = $deliveryDate;
                }
            }
        }

        $letters = Letter::all();
        $addresses = Address::query()->where('user_id', $user->getAttribute('id'))->with(['district', 'city'])->get();


        $special_discount = null;
        if (\auth()->check()) {
            if (Order::query()->where('user_id', \auth()->id())->count() == 0) {
                $special_discount = [
                    'title' => 'İlk sifariş (5%)',
                    'amount' => $cart->payed * 0.05
                ];
            }
        }

        return view('front.cart.checkout', compact('cart', 'user', 'cities', 'districts', 'prepTime', 'letters', 'addresses', 'special_discount'));

    }

    public function checkout(Request $request): View|RedirectResponse
    {
        $user = $this->client;
        if ($user) {
            $cart = Cart::query()->with(['cartCartItems', 'coupon'])
                ->withCount('cartCartItems')
                ->where(['user_id' => $user->getAttribute('id')])
                ->first();

            if (!$cart) {
                return redirect()->route('front');
            }
            $cart->calculate();

            $cities = null;
            $districts = null;
            $prepTime = null;
            $letters = null;
            $addresses = null;
            $special_discount = null;
            if (!$cart && !session()->has('payment')) {
                return redirect()->route('front');
            }

            if (session()->has('payment')) {
                $with = ['payment' => session()->get('payment'), 'order' => session()->get('order')];
                return view('front.cart.checkout', compact('cart', 'user', 'cities', 'districts', 'prepTime', 'letters', 'addresses', 'special_discount'))->with($with);
            }
        } else {
            return redirect()->route('front');
        }

        $cities = City::with('cityDistricts')->get();
        $currentDate = CarbonImmutable::now();
        $prepTime = null;
        $tecili_tarix = 0;
//        dd($cart->cartCartItems);
        foreach ($cart->cartCartItems as $key => $item) {
            if ($item->product->preperation_time) {

                if ($item->product->preperation_time_unit == 'days') {
                    $tecili_tarix += 24 * $item->product->preperation_time * $item->quantity;
                    $deliveryDate = $currentDate->addDays($item->product->preperation_time);
                } elseif ($item->product->preperation_time_unit == 'hours') {
//                    dd("KAmil");
                    $tecili_tarix += ($item->product->preperation_time * $item->quantity);
                    $deliveryDate = $currentDate->addHours($item->product->preperation_time);
                } else {
                    $deliveryDate = $currentDate;
                }

                if ($tecili_tarix > 2) {
                    $prepTime = $deliveryDate;
                }
            }
        }

        $letters = Letter::all();
        $addresses = Address::query()->where('user_id', $user->getAttribute('id'))->with(['district', 'city'])->get();


        $special_discount = null;
        if (\auth()->check()) {
            if (Order::query()->where('user_id', \auth()->id())->count() == 0) {
                $special_discount = [
                    'title' => 'İlk sifariş (5%)',
                    'amount' => $cart->payed * 0.05
                ];
            }
        }
        return view('front.cart.checkout2', compact('cart', 'user', 'cities', 'prepTime', 'letters', 'addresses', 'special_discount'));

    }
}
