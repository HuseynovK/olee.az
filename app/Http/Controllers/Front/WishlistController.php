<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $wishlists = Wishlist::with(['product.media'])
            ->where(['user_id' => $this->client->getAttribute('id')])
            ->latest()
            ->get();

        $view = view('front.wishlist.wishlist-card', compact('wishlists'))->render();

        return response()->json(['html' => $view, 'dac' => $wishlists->count()]);
    }

    public function index_other()
    {
        $products = Wishlist::with(['product.media', 'product' => function ($q) {
            $q->select(
                "id",
                "title",
                "description",
                "price",
                "discount_type",
                "discount",
                "preperation_time",
                "is_published",
                "is_featured",
                "is_valentine",
                "preperation_time_unit",
                "discount_expires_at",
                "delivery",
                "created_at",
                "category_id"
            );
        }])
            ->where(['user_id' => $this->client->getAttribute('id')])
            ->latest()
            ->get();
        return view('front.products.wishlist', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Product $product
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Product $product): JsonResponse
    {
        $wishlist = Wishlist::query()
            ->updateOrCreate([
                'user_id' => $this->client->getAttribute('id'),
                'product_id' => $product->getAttribute('id')
            ]);

        return response()->json([
            'success' => true,
            'data' => $wishlist,
            'errors' => null,
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function destroy(Product $product): JsonResponse
    {
        Wishlist::query()
            ->where([
                'user_id' => $this->client->getAttribute('id'),
                'product_id' => $product->getAttribute('id')
            ])->delete();

        return response()->json([
            'success' => true,
            'data' => null,
            'errors' => null,
        ], Response::HTTP_ACCEPTED);
    }
}
