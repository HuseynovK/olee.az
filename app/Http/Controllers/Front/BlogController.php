<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::query()->where('is_published', true)->latest()->paginate();
        return view("front.blogs.index")->with(compact('blogs'));
    }

    public function get(Blog $blog)
    {
        $others = Blog::query()
            ->where('id', '!=', $blog->getAttribute('id'))
            ->inRandomOrder()
            ->limit(2)
            ->get();

        return view("front.blogs.detail")->with(compact('blog', 'others'));
    }
}
