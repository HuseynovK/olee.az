<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\UpdateUserRequest;
use App\Models\Address;
use App\Models\City;
use App\Models\District;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user = User::with(['userAddresses'])->where(['id' => auth()->user()->id])->firstOrFail();
        return view('front.user.profile', compact('user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => [
                'nullable',
            ],
            'email' => [
                'nullable',
                'unique:users,email,' . auth()->user()->id,
            ],
        ]);
        $user = Auth::user();
        $user = User::where(['id' => $user->id])->first();
        $user->update([
            'name' => trim($request->input('name')),
            'email' => trim($request->input('email')),
        ]);
        if ($request->input('password')) {
            if ($request->input('password') == $request->input('confirm_password')) {
                User::where(['id' => $user->id])->update([
                    'password' => $request->input('password'),
                ]);
            } else {
                return back()->withErrors('invalid', translate('passwords not match'));
            }
        }
        return back()->with('updated', true);
    }


    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        if ($request->input('password')) {
            if ($request->input('password') == $request->input('confirm_password')) {
                User::where(['id' => $user->id])->update([
                    'password' => Hash::make($request->input('password')),
                ]);
            } else {
                return back()->withErrors('invalid', translate('passwords not match'));
            }
        }
        return back()->with('updated', true);
    }

    public function orders()
    {
        $user = User::with(['userOrders' => function ($query) {
            $query->where('orders.payment_status', 1);
            $query->orWhere('orders.payment_type', 'cash');
        }, 'userOrders.orderOrderDetails.product.media'])
            ->withCount(['userOrders' => function ($query) {
                $query->where('orders.payment_status', 1);
                $query->orWhere('orders.payment_type', 'cash');
            }])
            ->where(['id' => $this->client->getAttribute('id')])
            ->first();

        return view('front.user.orders.index', compact('user'));
    }


    public function addresses()
    {
        $user = \auth()->user();
        $cities = City::all();
        $districts = District::all();
        $addresses = Address::query()->where(['user_id' => \auth()->id()])->get();
        return view('front.user.addresses', compact('user', 'addresses', 'cities', 'districts'));
    }

    public function referrals()
    {
        $user = Auth::user();
        return view('front.user.referrals', compact('user'));
    }
}
