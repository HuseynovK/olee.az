<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use App\Services\AtlSms;
use App\Services\SmsService;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected AtlSms $smsService;
    protected Model|Builder|Authenticatable|null $client;

    public function __construct()
    {
        $this->smsService = new AtlSms();

        $this->middleware(function ($request, $next) {
            if (auth()->check()) {
                $this->client = auth()->user();
            } else {
                $session = session()->get('basket_token', function () {
                    $basket_token = Str::random(32);
                    session(['basket_token' => $basket_token]);
                    return $basket_token;
                });
                $this->client = User::query()->firstOrCreate(['session' => $session]);
            }
            $itemsCnt = 0;

            $cart = Cart::query()
                ->withCount('cartCartItems')
                ->where(['user_id' => $this->client->getAttribute('id')])
                ->first();

            if ($cart != null) {
                $itemsCnt = $cart->cart_cart_items_count;
            }

            view()->share('basketCnt', $itemsCnt);
            //            if (session()->has('banner')) {
            //                session(['banner' => 'hide']);
            //            } else {
            //                session(['banner' => 'show']);
            //            }

            //            session()->forget('banner');
            //session()->invalidate();
            //            dd(session()->get('banner'));

            return $next($request);
        });
    }

    public function preprocessNumber($phone): string
    {
        $phoneNumeric = str_replace(['-', '+', ' ','(',')'], '', $phone);
        $phoneCleaned = Str::substr($phoneNumeric, -9);
        return "994{$phoneCleaned}";
    }
}
