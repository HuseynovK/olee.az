<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyLetterRequest;
use App\Http\Requests\StoreLetterRequest;
use App\Http\Requests\UpdateLetterRequest;
use App\Models\Letter;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class LettersController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('letter_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $letters = Letter::with(['media'])->get();

        return view('admin.letters.index', compact('letters'));
    }

    public function create()
    {
        abort_if(Gate::denies('letter_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.letters.create');
    }

    public function store(StoreLetterRequest $request)
    {
        $letter = Letter::create($request->all());

        if ($request->input('image', false)) {
            $letter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $letter->id]);
        }

        return redirect()->route('admin.letters.index');
    }

    public function edit(Letter $letter)
    {
        abort_if(Gate::denies('letter_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.letters.edit', compact('letter'));
    }

    public function update(UpdateLetterRequest $request, Letter $letter)
    {
        $letter->update($request->all());

        if ($request->input('image', false)) {
            if (!$letter->image || $request->input('image') !== $letter->image->file_name) {
                if ($letter->image) {
                    $letter->image->delete();
                }

                $letter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($letter->image) {
            $letter->image->delete();
        }

        return redirect()->route('admin.letters.index');
    }

    public function show(Letter $letter)
    {
        abort_if(Gate::denies('letter_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $letter->load('letterOrders');

        return view('admin.letters.show', compact('letter'));
    }

    public function destroy(Letter $letter)
    {
        abort_if(Gate::denies('letter_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $letter->delete();

        return back();
    }

    public function massDestroy(MassDestroyLetterRequest $request)
    {
        Letter::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('letter_create') && Gate::denies('letter_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Letter();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
