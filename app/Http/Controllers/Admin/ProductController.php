<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Product::with(['category', 'colors', 'recommendation_impression'])->select(sprintf('%s.*', (new Product)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'product_show';
                $editGate = 'product_edit';
                $deleteGate = 'product_delete';
                $crudRoutePart = 'products';
                $affiliateGate = 'affiliate_access';

                return view('partials.datatablesActions', compact(
                    'affiliateGate',
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('category_title', function ($row) {
                return $row->category ? $row->category->title : '';
            });

            $table->editColumn('title', function ($row) {
                return '<a href="" target="_blank">' . $row->title ? $row->title : "Product - " . $row->id . '</a>';
            });
            $table->editColumn('price', function ($row) {
                return $row->price ? $row->price : "";
            });
            $table->editColumn('discount', function ($row) {
                return $row->discount ? $row->discount : "";
            });
            $table->editColumn('discount_type', function ($row) {
                return $row->discount_type ? Product::DISCOUNT_TYPE_SELECT[$row->discount_type] : '';
            });

            $table->editColumn('preperation_time', function ($row) {
                return $row->preperation_time ? $row->preperation_time : "";
            });
            $table->editColumn('preperation_time_unit', function ($row) {
                return $row->preperation_time_unit ? Product::PREPERATION_TIME_UNIT_SELECT[$row->preperation_time_unit] : '';
            });
            $table->editColumn('is_published', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_published ? 'checked' : null) . '>';
            });
            $table->editColumn('is_featured', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_featured ? 'checked' : null) . '>';
            });
            $table->editColumn('is_woman_day', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_woman_day ? 'checked' : null) . '>';
            });
            $table->editColumn('is_valentine', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_valentine ? 'checked' : null) . '>';
            });
            $table->editColumn('is_most_visited', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_most_visited ? 'checked' : null) . '>';
            });
            $table->editColumn('is_exclusive', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_exclusive ? 'checked' : null) . '>';
            });
            $table->editColumn('is_choosen', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_choosen ? 'checked' : null) . '>';
            });
            $table->editColumn('is_love', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_love ? 'checked' : null) . '>';
            });
            $table->editColumn('is_apologize', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_apologize ? 'checked' : null) . '>';
            });
            $table->editColumn('is_work', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_work ? 'checked' : null) . '>';
            });
            $table->editColumn('is_congrotulation', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->is_congrotulation ? 'checked' : null) . '>';
            });
//            $table->editColumn('is_birthday', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_birthday ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_for_love', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_for_love ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_be_sad', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_be_sad ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_child', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_child ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_anniversary', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_anniversary ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_own', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_own ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_opening', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_opening ? 'checked' : null) . '>';
//            });
//            $table->editColumn('is_wedding', function ($row) {
//                return '<input type="checkbox" disabled ' . ($row->is_wedding ? 'checked' : null) . '>';
//            });
            $table->editColumn('color', function ($row) {
                $labels = [];

                foreach ($row->colors as $color) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $color->title);
                }

                return implode(' ', $labels);
            });
            $table->editColumn('thumbnail', function ($row) {
                if ($photo = $row->thumbnail) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('thumbnail_url', function ($row) {
                if ($photo = $row->thumbnail) {
                    return $photo->url;
                }

                return '';
            });
            $table->editColumn('images', function ($row) {
                if (!$row->images) {
                    return '';
                }

                $links = [];

                foreach ($row->images as $media) {
                    $links[] = '<a href="' . $media->getUrl() . '" target="_blank"><img src="' . $media->getUrl('thumb') . '" width="50px" height="50px"></a>';
                }

                return implode(' ', $links);
            });
            $table->addColumn('recommendation_impression_title', function ($row) {
                return $row->recommendation_impression ? $row->recommendation_impression->title : '';
            });

            $table->rawColumns([
                'actions',
                'is_congrotulation',
                'is_work',
                'is_apologize',
                'is_love',
//                'is_birthday',
//                'is_for_love',
//                'is_be_sad',
//                'is_child',
//                'is_anniversary',
//                'is_own',
//                'is_opening',
//                'is_wedding',
                'placeholder', 'category',
                'is_published', 'is_featured',
                'is_woman_day', 'is_valentine',
                'is_exclusive', 'is_choosen',
                'is_most_visited', 'color',
                'thumbnail', 'thumbnail_url',
                'images', 'recommendation_impression',
            ]);

            return $table->make(true);
        }

        $categories = Category::get();
        $colors = Color::get();
        $products = Product::get();

        return view('admin.products.index', compact('categories', 'colors', 'products'));
    }

    public function create()
    {
        abort_if(Gate::denies('product_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::query()
            ->whereNotNull('parent_id')
            ->get()
            ->pluck('full_title', 'id')
            ->prepend(trans('global.pleaseSelect'), '');
        $colors = Color::all()->pluck('title', 'id');

        $recommendation_impressions = Product::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.products.create', compact('categories', 'colors', 'recommendation_impressions'));
    }

    public function store(StoreProductRequest $request)
    {
        //return $request->all();

        $product = Product::create($request->all());
        //return $request->input('variations', []);

        if ($request->has('has_variation')) {
            foreach ($request->input('variations', []) as $variation) {
                ProductVariation::create([
                    'product_id' => $product->id,
                    'title' => $variation['title'],
                    'description' => $variation['description'],
                    'price' => $variation['price'],
                    'discount' => $variation['discount'],
                    'stock' => $variation['stock'],
                ]);
            }
        }

        if ($request->has('has_attribute')) {
            foreach ($request->input('attributes', []) as $attribute) {
                $productAttribute = ProductAttribute::create([
                    'product_id' => $product->id,
                    'type' => $attribute['type'],
                    'title' => $attribute['title'],
                    'price' => isset($attribute['price']) ? $attribute['price'] : null,
                ]);
                if ($attribute['type'] == "select") {
                    foreach ($attribute['values'] as $value) {
                        ProductAttributeValue::create([
                            'product_attribute_id' => $productAttribute->id,
                            'title' => $value['title'],
                            'price' => $value['price'],
                        ]);
                    }
                }
            }
        }

        $product->colors()->sync($request->input('colors', []));

        if ($request->input('thumbnail', false)) {
            $product
                ->addMedia(storage_path('tmp/uploads/' . $request->input('thumbnail')))
                ->toMediaCollection('thumbnail');
        }

        foreach ($request->input('images', []) as $file) {
            $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('images');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $product->id]);
        }

        return redirect()->route('admin.products.index');
    }

    public function edit(Product $product)
    {
        abort_if(Gate::denies('product_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::query()
            ->whereNotNull('parent_id')
            ->get()
            ->pluck('full_title', 'id')
            ->prepend(trans('global.pleaseSelect'), '');

        $colors = Color::all()->pluck('title', 'id');

        $recommendation_impressions = Product::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $product->load('category', 'colors', 'recommendation_impression');

        return view('admin.products.edit', compact('categories', 'colors', 'recommendation_impressions', 'product'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        //return $product->productProductVariations;
        $product->update($request->all());
        $product->colors()->sync($request->input('colors', []));
        $product->productProductVariations()->forceDelete();
        ProductAttributeValue::whereIn('product_attribute_id', $product->productProductAttributes->pluck(['id']))->forceDelete();
        $product->productProductAttributes()->forceDelete();

        if ($request->has('has_variation')) {
            foreach ($request->input('variations', []) as $variation) {
                ProductVariation::create([
                    'product_id' => $product->id,
                    'title' => $variation['title'],
                    'description' => $variation['description'],
                    'price' => $variation['price'],
                    'discount' => $variation['discount'],
                    'stock' => $variation['stock'],
                ]);
            }
        }

        if ($request->has('has_attribute')) {
            foreach ($request->input('attributes', []) as $attribute) {
                $productAttribute = ProductAttribute::create([
                    'product_id' => $product->id,
                    'type' => $attribute['type'],
                    'title' => $attribute['title'],
                    'price' => isset($attribute['price']) ? $attribute['price'] : null,
                ]);
                if ($attribute['type'] == "select") {
                    foreach ($attribute['values'] as $value) {
                        ProductAttributeValue::create([
                            'product_attribute_id' => $productAttribute->id,
                            'title' => $value['title'],
                            'price' => $value['price'],
                        ]);
                    }
                }
            }
        }

        if ($request->input('thumbnail', false)) {
            if (!$product->thumbnail || $request->input('thumbnail') !== $product->thumbnail->file_name) {
                if ($product->thumbnail) {
                    $product->thumbnail->delete();
                }

                $product->addMedia(storage_path('tmp/uploads/' . $request->input('thumbnail')))->toMediaCollection('thumbnail');
            }
        } elseif ($product->thumbnail) {
            $product->thumbnail->delete();
        }

        if (count($product->images) > 0) {
            foreach ($product->images as $media) {
                if (!in_array($media->file_name, $request->input('images', []))) {
                    $media->delete();
                }
            }
        }

        $media = $product->images->pluck('file_name')->toArray();

        foreach ($request->input('images', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('images');
            }
        }

        return redirect()->route('admin.products.index');
    }

    public function show(Product $product)
    {
        abort_if(Gate::denies('product_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $product->load('category', 'colors', 'recommendation_impression', 'productProductVariations', 'productProductAttributes', 'recommendationImpressionProducts');

        return view('admin.products.show', compact('product'));
    }

    public function destroy(Product $product)
    {
        abort_if(Gate::denies('product_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $product->delete();

        return back();
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
        Product::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('product_create') && Gate::denies('product_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model = new Product();
        $model->id = $request->input('crud_id', 0);
        $model->exists = true;
        $media = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
