<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrdersOrderStatus;
use App\Models\User;
use App\Services\AtlSms;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use function Composer\Autoload\includeFile;

class OrderController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user = User::find(auth()->user()->id);
        $orders = Order::with(['user', 'address.city', 'address.district', 'letter', 'statuses']);

        if ($user->is_affiliate) {
            $orders->where('ref_id', $user->id);
        }
        $orders = $orders->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order->load('user', 'address', 'letter', 'orderOrderDetails', 'address.city', 'address.district')->append('calculateTotal');
        if (!$order->getAttribute('seen')) {
            $order->update(['seen' => true]);
        }

        return view('admin.orders.show', compact('order'));
    }

    public function setStatus(Order $order, Request $request): int|RedirectResponse|OrdersOrderStatus
    {
//        dd($request);
        if ($request->filled('status')) {
            $step = OrdersOrderStatus::query()
                ->with(['order_status'])
                ->where('order_id', $order->getAttribute('id'))
                ->whereNull('status')
                ->orderBy('number')
                ->first();
//dd($step);
            if ($step != null) {
                $step->setAttribute('status', $request->get('status'));
                $step->setAttribute('completed_at', date("Y-m-d H:i:s"));
                $step->save();
                //return $step->order_status;
                if ($request->get('status') == true && $step->order_status?->message != null && ($step->number == 4 || $step->number == 2)) {
                    try {
                        //return 55;
                        $message = str_replace('{order_number}', $order->getAttribute('order_number'), $step->order_status->message);
                        $order->load(['address']);
                        $phone = null;
//                        dd($order);
                        if (isset($order->sender['phone']) && $order->sender['phone'] != null) {
                            $phone = $order->sender['phone'];
                        } else {
                            $phone = $order->address->phone;
                        }
                        $phone = str_replace([' ', ')', '(', '+'], '', $phone);
//                        dd($phone);
                        (new AtlSms())->send($phone, $message);
                    } catch (\Exception $e) {
                        return back()->withErrors($e->getMessage());
                    }
                }
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        abort_if(Gate::denies('order_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        Order::query()->where('id', $id)->delete();
        return redirect()->back();
    }
}
