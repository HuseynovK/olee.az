<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCategoryAttributeRequest;
use App\Http\Requests\StoreCategoryAttributeRequest;
use App\Http\Requests\UpdateCategoryAttributeRequest;
use App\Models\Category;
use App\Models\CategoryAttribute;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryAttributeController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('category_attribute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categoryAttributes = CategoryAttribute::with(['category'])->get();

        return view('admin.categoryAttributes.index', compact('categoryAttributes'));
    }

    public function create()
    {
        abort_if(Gate::denies('category_attribute_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.categoryAttributes.create', compact('categories'));
    }

    public function store(StoreCategoryAttributeRequest $request)
    {
        $categoryAttribute = CategoryAttribute::create($request->all());

        return redirect()->route('admin.category-attributes.index');
    }

    public function edit(CategoryAttribute $categoryAttribute)
    {
        abort_if(Gate::denies('category_attribute_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categoryAttribute->load('category');

        return view('admin.categoryAttributes.edit', compact('categories', 'categoryAttribute'));
    }

    public function update(UpdateCategoryAttributeRequest $request, CategoryAttribute $categoryAttribute)
    {
        $categoryAttribute->update($request->all());

        return redirect()->route('admin.category-attributes.index');
    }

    public function show(CategoryAttribute $categoryAttribute)
    {
        abort_if(Gate::denies('category_attribute_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categoryAttribute->load('category');

        return view('admin.categoryAttributes.show', compact('categoryAttribute'));
    }

    public function destroy(CategoryAttribute $categoryAttribute)
    {
        abort_if(Gate::denies('category_attribute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categoryAttribute->delete();

        return back();
    }

    public function massDestroy(MassDestroyCategoryAttributeRequest $request)
    {
        CategoryAttribute::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
