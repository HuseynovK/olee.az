<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCategoryRequest;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('category_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::with(['media', 'parent'])
            ->when(\request()->filled('parent_id'), function ($query){
                return $query->where('parent_id', \request()->get('parent_id'));
            })
            ->when(!\request()->filled('parent_id'), function ($query){
                return $query->whereNull('parent_id');
            })
            ->get();

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        abort_if(Gate::denies('category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.categories.create')->with(compact('categories'));
    }

    public function store(StoreCategoryRequest $request)
    {
//        dd("Kamil");
        $category = Category::create($request->all());
        if ($request->input('image', false)) {
            $category->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }

        if ($request->input('navbar', false)) {
            $category->addMedia(storage_path('tmp/uploads/' . $request->input('navbar')))->toMediaCollection('navbar');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $category->id]);
        }

        return redirect()->route('admin.categories.index');
    }

    public function edit(Category $category)
    {
        abort_if(Gate::denies('category_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.categories.edit', compact('category', 'categories'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        if ($request->filled('parent_id'))
        {
            if ($category->children()->count() > 0)
            {
                throw ValidationException::withMessages(['parent_id' => 'This category has subcategories, you can not set a parent to it.']);
            }
        }
        $category->update($request->all());

        if ($request->input('image', false)) {
            if (!$category->image || $request->input('image') !== $category->image->file_name) {
                if ($category->image) {
                    $category->image->delete();
                }

                $category->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($category->image) {
            $category->image->delete();
        }

        if ($request->input('navbar', false)) {
            if (!$category->navbar || $request->input('navbar') !== $category->navbar->file_name) {
                if ($category->navbar) {
                    $category->navbar->delete();
                }

                $category->addMedia(storage_path('tmp/uploads/' . $request->input('navbar')))->toMediaCollection('navbar');
            }
        } elseif ($category->navbar) {
            $category->navbar->delete();
        }

        return redirect()->route('admin.categories.index');
    }

    public function show(Category $category)
    {
        abort_if(Gate::denies('category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $category->load('categoryProducts', 'categoryCategoryAttributes');

        return view('admin.categories.show', compact('category'));
    }

    public function destroy(Category $category)
    {
        abort_if(Gate::denies('category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $category->delete();

        return back();
    }

    public function massDestroy(MassDestroyCategoryRequest $request)
    {
        Category::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('category_create') && Gate::denies('category_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Category();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
