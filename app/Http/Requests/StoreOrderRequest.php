<?php

namespace App\Http\Requests;

use App\Models\Order;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreOrderRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('order_create');
    }

    public function rules()
    {
        return [
            'address_id'     => [
                'required',
                'integer',
            ],
            'order_number'   => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'unique:orders,order_number',
            ],
            'delivery_date'  => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'delivery_time'  => [
                'string',
                'nullable',
            ],
            'payment_type'   => [
                'required',
            ],
            'note'           => [
                'string',
                'max:500',
                'nullable',
            ],
            'payment_status' => [
                'required',
            ],
        ];
    }
}
