<?php

namespace App\Http\Requests;

use App\Models\ProductAttributeValue;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateProductAttributeValueRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_attribute_value_edit');
    }

    public function rules()
    {
        return [
            'product_attribute_id' => [
                'required',
                'integer',
            ],
            'title'                => [
                'string',
                'min:1',
                'required',
            ],
        ];
    }
}
