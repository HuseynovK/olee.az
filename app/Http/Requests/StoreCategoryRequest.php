<?php

namespace App\Http\Requests;

use App\Models\Category;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class StoreCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('category_create');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'min:3',
                'max:100',
                'required',
                'unique:categories',
            ],
            'parent_id' => [
                'nullable',
                'sometimes',
                Rule::exists('categories', 'id')
                    ->whereNull('deleted_at')
                    ->whereNull('parent_id')
            ]
        ];
    }
}
