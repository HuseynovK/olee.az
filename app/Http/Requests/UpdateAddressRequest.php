<?php

namespace App\Http\Requests;

use App\Models\Address;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateAddressRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('address_edit');
    }

    public function rules()
    {
        return [
            'address_id' => [
                'required'
            ],
            'district_id' => [
                'required'
            ],
            'title' => [
                'nullable',
                'sometimes',
                'string',
                'min:2',
                'max:50',
            ],
            'detail' => [
                'string',
                'min:2',
                'max:255',
                'required',
            ],
            'full_name' => [
                'string',
                'min:2',
                'max:100',
                'required',
            ],
            'phone' => [
                'string',
                'min:7',
                'max:25',
                'required',
            ],
        ];
    }
}
