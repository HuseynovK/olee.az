<?php

namespace App\Http\Requests;

use App\Models\ContactMessage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateContactMessageRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('contact_message_edit');
    }

    public function rules()
    {
        return [
            'firstname' => [
                'string',
                'min:5',
                'required',
            ],
            'lastname'  => [
                'string',
                'min:5',
                'required',
            ],
            'phone'     => [
                'string',
                'min:7',
                'max:25',
                'required',
            ],
            'subject'   => [
                'string',
                'max:50',
                'required',
            ],
        ];
    }
}
