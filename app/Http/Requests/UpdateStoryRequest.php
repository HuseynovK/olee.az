<?php

namespace App\Http\Requests;

use App\Models\Story;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateStoryRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('story_edit');
    }

    public function rules()
    {
        return [
            'author'     => [
                'string',
                'min:5',
                'max:50',
                'required',
            ],
            'profession' => [
                'string',
                'min:5',
                'max:55',
                'required',
            ],
        ];
    }
}
