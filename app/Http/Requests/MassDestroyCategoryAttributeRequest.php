<?php

namespace App\Http\Requests;

use App\Models\CategoryAttribute;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCategoryAttributeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('category_attribute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:category_attributes,id',
        ];
    }
}
