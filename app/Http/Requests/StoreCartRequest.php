<?php

namespace App\Http\Requests;

use App\Models\Cart;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCartRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('cart_create');
    }

    public function rules()
    {
        return [
            'session' => [
                'string',
                'nullable',
            ],
            'total'   => [
                'numeric',
                'min:1',
            ],
            'payed'   => [
                'numeric',
                'min:1',
                'max:100000',
            ],
        ];
    }
}
