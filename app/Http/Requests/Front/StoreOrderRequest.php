<?php

namespace App\Http\Requests\Front;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class StoreOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true; //Gate::allows('order_create');
    }

    public function rules()
    {
        $rules = [
//            'address_id' => ['required', 'exists:addresses,id'],
            'paymentType' => ['required', 'in:cash,online'],
//            'isGift' => ['required', 'in:0,1'],
        ];

//        if ($this->attributes->get('isGift')) {
            $rules = array_merge($rules, [
                'sender_firstname' =>  ['required'],
                'sender_phone' =>  ['required'],
            ]);
//        }

        if (!$this->attributes->get('isUrgent')) {
            $rules = array_merge($rules, [
                'deliveryDate' =>  ['nullable'],
                'deliveryTime' =>  ['nullable'],
            ]);
        }

//        if ($this->attributes->get('letter_id')) {
//            $rules = array_merge($rules, [
//                'letter_id' =>  ['exists:letters,id'],
//            ]);
//        }
        return $rules;
    }
    public function messages()
    {
        return [
            'paymentType.required'=>"Zəhmət olmasa ödəniş üsulunu seçin",
            'sender_firstname.required'=>"Zəhmət olmasa Ad Soyad bölməsin doldurun",
            'sender_phone.required'=>"Zəhmət olmasa Telefon daxil edin",
            'deliveryDate.required'=>"Zəhmət olmasa Çatdırılma tarixin seçin",
            'deliveryTime.required'=>"Zəhmət olmasa Çatdırılma vaxtın seçin",
        ];
    }
}
