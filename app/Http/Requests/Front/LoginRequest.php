<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->filled('phone')) {
            $phone = Str::substr(str_replace(['-', '+', ' ','(',')'], '', $this->get('phone')), -9);
            $this->merge(['phone' => "994{$phone}"]);
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|exists:users,phone',
            'password' => 'required|string|min:6|max:30',
        ];
    }
}
