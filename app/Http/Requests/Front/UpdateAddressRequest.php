<?php

namespace App\Http\Requests\Front;

use App\Models\Address;
use App\Rules\DistrictMustBeRequiredORNotRule;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateAddressRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'city_id' => [
                'required',
                'integer',
            ],
            'district_id' => [
                new DistrictMustBeRequiredORNotRule($this->city_id)
            ],
            'title' => [
                'string',
                'min:2',
                'max:50',
                'required',
            ],
            'detail' => [
                'string',
                'min:2',
                'max:255',
                'required',
            ],
            'full_name' => [
                'string',
                'min:2',
                'max:100',
                'required',
            ],
            'phone' => [
                'string',
                'min:7',
                'max:25',
                'required',
            ],
        ];
    }
}
