<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => [
                'string',
                'min:5',
                'required',
            ],
            'lastname'  => [
                'string',
                'min:5',
                'required',
            ],
            'phone'     => [
                'string',
                'min:7',
                'max:25',
                'required',
            ],
            'subject'   => [
                'string',
                'max:50',
                'required',
            ],
        ];
    }
}
