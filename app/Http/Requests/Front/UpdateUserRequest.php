<?php

namespace App\Http\Requests\Front;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'              => [
                'string',
                'required',
            ],
            'phone'             => [
                'string',
                'required',
            ],
            'email'             => [
                'required',
                'unique:users,email,' . auth()->user()->id,
            ],
        ];
    }
}
