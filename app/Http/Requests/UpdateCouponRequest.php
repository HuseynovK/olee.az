<?php

namespace App\Http\Requests;

use App\Models\Coupon;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UpdateCouponRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('coupon_edit');
    }

    public function rules()
    {
        return [
            'title'         => [
                'string',
                'required',
            ],
            'discount'      => [
                'numeric',
                'required',
                'min:1',
                'max:100000',
            ],
            'discount_type' => [
                'required',
            ],
            'coupon_type' => [
                'required',
                Rule::in(array_keys(Coupon::COUPON_TYPE_SELECT))
            ],
            'expire_date' => [
                'nullable',
                'sometimes',
                'date',
                'after:today'
            ],
            'count' => [
                'nullable',
                'sometimes',
                'integer',
                'min:1'
            ]
        ];
    }
}
