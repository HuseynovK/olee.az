<?php

namespace App\Http\Requests;

use App\Models\Product;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreProductRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_create');
    }

    public function rules()
    {
        return [
            'category_id'         => [
                'required',
                'integer',
            ],
            'title'               => [
                'string',
                'min:1',
                'max:255',
                'required',
            ],
            'price'               => [
                'required',
            ],
            'discount'            => [
                'numeric',
                'min:0',
                'max:100000',
            ],
            'discount_expires_at' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
            'preperation_time'    => [
                'numeric',
                'nullable'
            ],
            'colors.*'            => [
                'integer',
            ],
            'colors'              => [
                'array',
            ],
            'thumbnail'           => [
                'required',
            ],
        ];
    }
}
