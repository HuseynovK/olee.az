<?php

namespace App\Http\Requests;

use App\Models\Address;
use App\Rules\DistrictMustBeRequiredORNotRule;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Nullable;

class StoreAddressRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'district_id' => [
                'required'
            ],
            'title' => [
                'nullable',
                'sometimes',
                'string',
                'min:2',
                'max:50',
            ],
            'detail' => [
                'string',
                'min:2',
                'max:255',
                'required',
            ],
            'full_name' => [
                'string',
                'min:2',
                'max:100',
                'required',
            ],
            'phone' => [
                'string',
                'min:7',
                'max:25',
                'required',
            ],
        ];
    }
}
