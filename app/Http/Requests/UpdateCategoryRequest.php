<?php

namespace App\Http\Requests;

use App\Models\Category;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UpdateCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('category_edit');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'min:3',
                'max:100',
                'required',
                'unique:categories,title,' . request()->route('category')->id,
            ],
            'parent_id' => [
                'nullable',
                'sometimes',
                Rule::exists('categories', 'id')
                    ->whereNull('parent_id')->whereNot('id', request()->route('category')->id)
                    ->whereNull('deleted_at')
            ]
        ];
    }
}
