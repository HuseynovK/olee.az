<?php

namespace App\Http\Requests;

use App\Models\CategoryAttribute;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCategoryAttributeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('category_attribute_edit');
    }

    public function rules()
    {
        return [
            'category_id' => [
                'required',
                'integer',
            ],
            'title'       => [
                'string',
                'required',
                'unique:category_attributes,title,' . request()->route('category_attribute')->id,
            ],
        ];
    }
}
