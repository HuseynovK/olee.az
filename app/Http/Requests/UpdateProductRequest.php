<?php

namespace App\Http\Requests;

use App\Models\Product;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateProductRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_edit');
    }

    public function rules()
    {
        return [
            'category_id'         => [
                'required',
                'integer',
            ],
            'title'               => [
                'string',
                'min:1',
                'max:255',
                'required',
            ],
            'price'               => [
                'required',
            ],
            'discount'            => [
                'nullable',
                'numeric',
                'min:0',
                'max:100000',
            ],
            'discount_expires_at' => [
                'nullable',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'preperation_time'    => [
                'numeric',
                'nullable'
            ],
            'colors.*'            => [
                'integer',
            ],
            'colors'              => [
                'array',
            ],
        ];
    }
}
