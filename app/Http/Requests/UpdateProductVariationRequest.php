<?php

namespace App\Http\Requests;

use App\Models\ProductVariation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateProductVariationRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_variation_edit');
    }

    public function rules()
    {
        return [
            'product_id'  => [
                'required',
                'integer',
            ],
            'title'       => [
                'string',
                'min:1',
                'max:50',
                'required',
            ],
            'description' => [
                'string',
                'min:1',
                'max:60',
                'nullable',
            ],
            'stock'       => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
