<?php

namespace App\Providers;

use App\Http\View\Composer\AdminComposer;
use App\Http\View\Composer\FrontComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('front.partials.header', FrontComposer::class);
        View::composer('partials.menu', AdminComposer::class);
    }
}
