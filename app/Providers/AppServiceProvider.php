<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Content;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->environment(['production'])) {
            URL::forceScheme('https');
        }
        View::composer('front.static.nav', function ($view) {
            $view->with('data', ['menu' => Content::all()]);
        });
        View::composer('front.partials.header', function ($view) {
            $view->with('data', ['category' => Category::whereNull('parent_id')->with('children')->get()]);
        });
        if (app()->environment(['local', 'staging'])) {
            // The environment is either local OR staging...
            \DB::enableQueryLog();

            \DB::listen(
                function ($sql) {
                    foreach ($sql->bindings as $i => $binding) {
                        if ($binding instanceof \DateTime) {
                            $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                        } else {
                            if (is_string($binding)) {
                                $sql->bindings[$i] = "'$binding'";
                            }
                        }
                    }

                    // Insert bindings into query
                    $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);
                    $query = vsprintf($query, $sql->bindings);

                    // Save the query to file
                    $logFile = fopen(
                        storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '_query.log'),
                        'a+'
                    );
                    fwrite($logFile, $query . PHP_EOL);
                    fclose($logFile);
                });
        }
    }
}
