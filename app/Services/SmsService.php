<?php


namespace App\Services;

use Twilio\Rest\Client;
use App\Services\IServices\ISmsService;
use App\Services\ServiceResources\ServiceSmsRecipient;

class SmsService implements ISmsService
{

    const StatusSuccess = 200;
    const StatusErr = 400;

    public function send(string $phone, string $text): object
    {
        try {
            $client = new Client(env('TWILIO_SID'), env('TWILIO_AUTH_TOKEN'));

            $message = $client->messages->create(
                $phone,
                [
                    'from' => env('TWILIO_NUMBER'),
                    'body' => $text
                ]
            );

            if (isset($message->sid)) {
                //log response
                return (object)['status' => self::StatusSuccess, 'data' => $message->sid];
            }

        } catch (\Throwable $th) {
            //TODO log err
            return (object)['status' => self::StatusErr, 'message' => $th->getMessage()];
        }

        return (object)['status' => self::StatusErr, 'message' => 'Error occurs'];
    }
}
