<?php

namespace App\Services;

use App\Models\Sms;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use SimpleXMLElement;

class AtlSms // extends TwilioClient
{
    /**
     * @param string|null $phoneNumber
     * @param string $text
     * @return Model|Builder|null
     */
    public function send(string $phoneNumber, string $text): Model|Builder|null
    {
        if ($phoneNumber == null)
            return null;

        $sms = Sms::query()->create([
            'phone' => $phoneNumber,
            'message' => $text
        ]);

        $xmlObject = simplexml_load_file(public_path("/xml/atl.xml"));
        $xmlObject->head->login = Sms::ATL_USER;
        $xmlObject->head->password = Sms::ATL_PASS;
        $xmlObject->head->title = Sms::ATL_TITLE;
        $xmlObject->head->controlid = str_pad($sms->getAttribute('id'), 6, 0, STR_PAD_LEFT) . time();
        $xmlObject->body->msisdn = $phoneNumber;
        $xmlObject->body->message = $text;

        $response = Http::withHeaders([
            'Content-Type' => 'text/xml; charset=UTF8'
        ])->withBody($xmlObject->asXML(), 'text/xml; charset=UTF8')
            ->post(Sms::ATL_URL)->body();
        $sms->setAttribute('response', $response);
        $sms->save();

        return $sms;

    }

    /**
     * @param User $user
     * @param $text
     * @return Builder|Model
     */
    public function sendSmsToUser(User $user, $text): Builder|Model
    {
        $sms = $this->sendSms($user->getAttribute('phone'), $text);
        $sms->setAttribute('user_id', $user->getAttribute('id'));
        $sms->save();
        return $sms;
    }
}
