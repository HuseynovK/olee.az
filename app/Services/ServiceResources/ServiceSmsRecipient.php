<?php


namespace App\Services\ServiceResources;


use Illuminate\Database\Eloquent\Model;

class ServiceSmsRecipient extends Model
{
    protected $fillable = ['phone'];
}
