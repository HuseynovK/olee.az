<?php

namespace App\Services;

use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;

class Cart
{
    /**
     * @param string $productId
     * @param string $request
     */
    public function calculate(int $id, object $request)
    {
        $product = Product::with('category', 'colors', 'productProductAttributes', 'productProductVariations')->findOrFail($id);

        $price = $product->price;
        $discount = 0;
        if ($product->has('productProductVariations') && $request->input('product_variation')) {
            $pV = $product->productProductVariations()->find($request->input('product_variation'));
            $price = $pV->price;
        }

        if ($product->discount > 0 && ($product->discount_expires_at && strtotime($product->discount_expires_at) > time())) {
            if ($product->discount_type == 'flat') {
                $discount = $product->discount;
            }

            if ($product->discount_type == 'percentage') {
                $discount = $price * $product->discount * 0.01;
            }
        }


        $vPrices = 0;
        if ($product->has('productProductAttributes') && $request->input('attributeValue')) {
            foreach ($request->input('attributeValue') as $key => $attribute) {
                foreach ($attribute as $kk => $item) {
                    if ($key == 'select') {
                        $aV = ProductAttributeValue::where(['id' => $item, 'product_attribute_id' => $kk])->first();
                        $vPrices += $aV->price;
                    }

                    if ($key == 'input' || $key == 'file' && $item != null) {
                        $aV = ProductAttribute::where(['id' => $kk])->first();
                        $vPrices += $aV->price;
                    }
                }
            }
        }

        if ($request->file('attributeValue')) {
            $file2 = $request->file('attributeValue')['file'];
            foreach ($file2 as $kk => $item) {
                $aV = ProductAttribute::where(['id' => $kk])->first();
                $vPrices += $aV->price;
            }
        }

        return array(
            'net' => number_format((float)($price + $vPrices), 2, '.', ''),
            'payable' => number_format((float)$price + $vPrices - $discount, 2, '.', '')
        );
    }
}
