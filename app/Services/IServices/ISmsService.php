<?php


namespace App\Services\IServices;


use App\Services\ServiceResources\ServiceSmsRecipient;

interface ISmsService
{
    public function send(string $phone, string $message): object;
}
